// Type definitions for Login
// Project Connex
// Definitions by: Jaha Naeem Gitonga
import { APIGatewayProxyResult } from 'aws-lambda';
import { User } from './user';

export interface LoginCredentials {
    username: string;
    password: string;
}

export interface PasswordReset {
    resetWithToken?: boolean;
    oldPassword?: string;
    newPassword: string;
    confirmedPassword: string;
}

export type TokenResponse =
    | AccessTokenValidationResponse
    | DefaultTokenErrorResponse;

interface DefaultTokenErrorResponse {
    status: number;
}
interface AccessTokenValidationResponse {
    status: number;
    user: User;
}

export type DecodedToken =
    | DecodedAccessToken
    | DecodedXsrfToken
    | DecodedRefreshToken;

type DecodedAccessToken = {
    user: User;
    iat: number;
    exp: number;
    iss: string;
    sub: string;
};

interface DecodedXsrfToken {
    //Todo: create interface
}

interface DecodedRefreshToken {
    //Todo: create interface
}

export type AuthResponse = {
    payload: object | string;
    status: number;
    error: null | unknown | Error;
    headers: { [header: string]: string };
    multiValueHeader: APIGatewayProxyResult['multiValueHeaders'];
};

// export type MultiValueHeader

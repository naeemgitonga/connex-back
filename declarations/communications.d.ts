export interface CommunicationsResponse {
    message: string;
    status: number;
    error?: Error;
    MessageId?: string;
}

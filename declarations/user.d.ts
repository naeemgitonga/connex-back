// Type definitions for Users
// Project Connex
// Definitions by: Jaha Naeem Gitonga
import { UserRoles } from './globalEnums';
import { ObjectId } from 'mongodb';

export interface User {
    _id: ObjectId;
    username: string;
    password?: string;
    firstName: string;
    lastName: string;
    phone: string;
    img: null | string;
    companyId: ObjectId;
    role: UserRoles;
    __v?: number;
}

interface CompanyId {
    _id: ObjectId;
    role: number;
    permissions: string | string[]; //! this may be handled by the role
}

// Type definitions for Carrier
// Project Connex
// Definitions by: Jaha Naeem Gitonga

import { ObjectId } from 'mongodb';
import { CompanyType } from './globalEnums';
import { Load } from './load';
import { User } from './user';

export interface Company {
    _id: ObjectId;
    name: string;
    phone: string;
    type: CompanyType;
    deleted: boolean;
    address: {
        line1: string;
        line2: null | string;
        line3: null | string;
        city: string;
        state: string;
        zip: string;
    };
    __v?: number;
    //* we get the following when aggregating data
    admin?: User[];
    clerks?: User[];
    drivers?: User[];
    inboundLoads?: Load[];
    outboundLoads?: Load[];
    unsubmittedLoads?: Load[];
}

export type RouteMap = {
    GET?: {
        [name: string]: string;
    };
    POST?: {
        [name: string]: string;
    };
    PUT?: {
        [name: string]: string;
    };
    DELETE?: {
        [name: string]: string;
    };
};

export type ParamMap = {
    [name: string]: ObjectId;
};

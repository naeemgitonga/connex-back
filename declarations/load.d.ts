// Type definitions for Load
// Project Connex
// Definitions by: Jaha Naeem Gitonga
import { ObjectId } from 'mongodb';

export interface Load {
    _id: ObjectId;
    shipperId: ObjectId; // * id of company moving the load
    createdBy: ObjectId; // * person that created the load
    carrierId: ObjectId;
    ready: boolean;
    deleted: boolean;
    stops: number;
    pUDate: Date;
    pUNum: string;
    createdDate: Date;
    checkin: null | Date;
    checkout: null | Date;
    bols: ObjectId[];
    __v?: number;
}

export interface Bol {
    _id: ObjectId;
    file: File;
    shipperId: ObjectId;
    driverId: ObjectId;
    receiverId: ObjectId;
    checkin: null | Date;
    checkout: null | Date;
    dropDate: Date;
    osd: OSD;

    load?: Load; // * only used for driver checkin
}

interface File {
    lastModified: number;
    lastModifiedDate: Date;
    name: string;
    size: number;
    type: string;
    webkitRelativePath: string;
}
export interface OSD {
    overages: Item[];
    shortages: Item[];
    damages: Item[];
}
export interface Item {
    sku: string;
    qty: number;
}

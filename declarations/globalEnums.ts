//! if you try to put me in the .d.ts file it will break the modules resolution...idky
//! as a result this file was born to maintain consistency
export enum LoginErrors {
    IncorrectPassword = 'The original password that you supplied was incorrect!',
    Unauthorized = "We can't authenticate you!",
    UserNotFound = 'User does not exist!',
    NotFound = "We can't find it",
    CantDo = "We can't do that here",
    ItBroke = 'Something broke',
}

export enum ServerErrors {
    ItBroke = 'Something broke',
}

export interface Cookies {
    [name: string]: string;
}

export enum TokenOptions {
    Xsrf = 'xsrf',
    Refresh = 'refresh',
    Access = 'access',
    Algorithm = 'HS256',
    Issuer = 'connexapp.com',
    PasswordReset = 'password',
}

export enum JwtErrors {
    Malformed = 'jwt malformed',
    Expired = 'jwt expired',
}

export enum TokenNames {
    Access = 'ACCESS-TOKEN',
    Refresh = 'REFRESH-TOKEN',
    Xsrf = 'XSRF-TOKEN',
}

export enum Collections {
    Loads = 'loads',
    Bols = 'bols',
    Users = 'users',
    Carriers = 'carriers',
    Companys = 'companies',
}

export enum UserRoles {
    Admin = 1,
    Clerk = 2,
    Driver = 3,
}

//* Remember: these values start at 0, so Carrier = 0,
//* ShipperReceiver = 1
export enum CompanyType {
    Carrier,
    ShipperReceiver,
}

export enum HttpMethods {
    Get = 'GET',
    Post = 'POST',
    Delete = 'DELETE',
    Put = 'PUT',
}

export enum ImageType {
    Profile = 'profile',
}

export enum SuccessMessages {
    PasswordReset = 'You have successfully reset your password!',
}

export enum LoadsRoutes {
    getLoadsPagination = '/api/loads/pagination',
    getLoadById = '/api/loads/{id}',
    getLoads = '/api/loads',
    getLoadsByCarrier = '/api/loads/carrier/{id}',
    getLoadsByDriver = '/api/loads/driver/{id}',
    createLoad = '/api/loads',
    updateLoad = '/api/loads/{id}',
    deleteLoad = '/api/loads/{id}',
}

export enum CompaniesRoutes {
    getCompanies = '/api/companies/search',
    getCompany = '/api/companies/{id}',
    getCompanyLoadsUsers = '/api/companies/users/loads',
    getCompaniesByIds = '/api/companies/shippers-receivers-carriers',
    getAllLoads = '/api/companies/loads/outbound/inbound/complete/unsubmitted',
    createCompany = '/api/companies',
    companies = '/api/companies/{id}',
    updateCompany = '/api/companies/{id}',
    deleteCompany = '/api/companies/{id}',
}

export enum UsersRoutes {
    getUserById = '/api/users/{id}',
    getUsers = '/api/users',
    getOptions = '/api/users/options', // * I didn't want to do this...see below
    // refreshUser = '/api/users', //? I'm not sure why I made this...
    createUser = '/api/users',
    updateUser = '/api/users/{id}',
    deleteUser = '/api/users/{id}',
}

export enum BolsRoute {
    getLoadsForCarrier = '/api/bols/{id}',
    getPaginatedCompleted = '/api/bols/pagination',
    getBolsForDriver = '/api/bols/driver/{id}',
    updateBol = '/api/bols/{id}',
    updateBolsDrivers = '/api/bols/driver',
    deleteBol = '/api/bols/{id}',
}

export enum AuthRoutes {
    logout = '/api/auth/logout',
    refresh = '/api/auth/refresh',
    passwordResetEmail = '/api/auth/password/email',
    validateResetPasswordToken = '/api/auth/password/validate-token',
    login = '/api/auth/login',
    resetPassWordUnauthenticated = '/api/auth/password/reset/{id}',
    passwordResetByUserId = '/api/auth/password/{id}',
}

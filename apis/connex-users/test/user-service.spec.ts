import { APIGatewayEvent } from 'aws-lambda';
import { Collection, MongoClient, ObjectId } from 'mongodb';
import { UserRoles } from '../../../declarations/globalEnums';
import { User } from '../../../declarations/user';
import { UsersService } from '../users-service';

const DB__CONNECTION = process.env.DB__CONNECTION;

describe('Users service tests', () => {
    let collection: Collection;
    let client: MongoClient;
    const id = new ObjectId();
    const requestingUser = { firstName: 'jaha naeem' } as User;
    beforeAll(async () => {
        try {
            client = await MongoClient.connect(DB__CONNECTION, {
                useNewUrlParser: true,
                useUnifiedTopology: true,
            });
            const db = client.db('connex');
            collection = db.collection('users');
        } catch (e) {
            console.log(e);
            throw new Error(e.errmsg);
        }
    });

    afterAll(async () => {
        try {
            await collection.deleteMany({ firstName: 'Test-user' });
            await collection.updateOne(
                { _id: new ObjectId('5fab473cd1d1414f61298362') },
                { $set: { lastName: 'testing' } }
            );
            client.close();
        } catch (e) {
            console.log(e);
            throw new Error(e.errmsg);
        }
    });

    test('Should map request to method for singleton route: ', () => {
        const mockEvent = {
            path: '/api/users/59cd4bf28c25a93904f6d401',
            httpMethod: 'GET',
            body: JSON.stringify({}),
        } as APIGatewayEvent;
        const mockCollection = {} as Collection;
        const usersService = new UsersService(
            mockEvent,
            mockCollection,
            requestingUser
        );
        expect(usersService.mapRequestToMethod()).toBe('getUserById');
    });

    test('Should map request to method with param in route: ', () => {
        const mockEvent = {
            path: '/api/users',
            httpMethod: 'POST',
            body: JSON.stringify({}),
        } as APIGatewayEvent;
        const mockCollection = {} as Collection;
        const usersService = new UsersService(
            mockEvent,
            mockCollection,
            requestingUser
        );
        expect(usersService.mapRequestToMethod()).toBe('createUser');
    });

    test('Should map request to method: ', () => {
        const mockEvent = {
            path: '/api/users/59cd4bf28c25a93904f6d401',
            httpMethod: 'PUT',
            body: JSON.stringify({}),
        } as APIGatewayEvent;
        const mockCollection = {} as Collection;
        const usersService = new UsersService(
            mockEvent,
            mockCollection,
            requestingUser
        );
        expect(usersService.mapRequestToMethod()).toBe('updateUser');
    });

    test('Should NOT map request to method with params in route: ', () => {
        const mockEvent = {
            path:
                '/api/users/59cd4bf28c25a93904f6d401/admin/59cd4bf28c25a93904f6d401/users',
            httpMethod: 'GET',
            body: JSON.stringify({}),
        } as APIGatewayEvent;
        const mockCollection = {} as Collection;
        const usersService = new UsersService(
            mockEvent,
            mockCollection,
            requestingUser
        );
        try {
            usersService.mapRequestToMethod();
        } catch (e) {
            const error = `We don't have a method to handle this request: /api/users/59cd4bf28c25a93904f6d401/admin/59cd4bf28c25a93904f6d401/users`;
            expect(e.message).toMatch(error);
        }
    });

    test('Should make a user: ', async () => {
        const mockEvent = {
            path: '/api/users',
            httpMethod: 'POST',
            body: JSON.stringify({
                _id: id,
                username: 'test-user@nconnex.com',
                password: 'asdfeo93law893902398230923',
                firstName: 'Test-user',
                lastName: 'User',
                phone: '4046700059',
                img: null,
                companyId: new ObjectId(),
                role: UserRoles.Driver,
            }),
        } as APIGatewayEvent;

        const usersService = new UsersService(
            mockEvent,
            collection,
            requestingUser
        );
        const whichMethod = usersService.mapRequestToMethod();
        const { result } = await usersService[whichMethod]();
        expect(result.n).toBe(1);
        expect(result.ok).toBe(1);
    });

    test('Should update a user: ', async () => {
        const update = {
            lastName: 'User2',
        };
        const mockEvent = {
            path: `/api/users/5fab473cd1d1414f61298362`,
            httpMethod: 'PUT',
            body: JSON.stringify(update),
        } as APIGatewayEvent;

        const usersService = new UsersService(
            mockEvent,
            collection,
            requestingUser
        );
        const whichMethod = usersService.mapRequestToMethod();
        const result = await usersService[whichMethod]();
        expect(result).toBeTruthy();
        expect(result.lastName).toBe(update.lastName);
    });

    test('Should findUserById: ', async () => {
        const mockEvent = {
            path: `/api/users/59ca7ee930370c343cdd2382`,
            httpMethod: 'GET',
            body: JSON.stringify({}),
        } as APIGatewayEvent;

        const usersService = new UsersService(
            mockEvent,
            collection,
            requestingUser
        );
        const whichMethod = usersService.mapRequestToMethod();
        expect(usersService.mapRequestToMethod()).toBe('getUserById');
        const result = await usersService[whichMethod]();
        expect(result).toBeTruthy();
    });

    test('Should delete a user: ', async () => {
        const mockEvent = {
            path: `/api/users/${id}`,
            httpMethod: 'DELETE',
            body: JSON.stringify({
                phone: '4046700060',
            }),
        } as APIGatewayEvent;

        const usersService = new UsersService(
            mockEvent,
            collection,
            requestingUser
        );
        const whichMethod = usersService.mapRequestToMethod();
        const { result } = await usersService[whichMethod]();
        expect(result.n).toBe(1);
        expect(result.ok).toBe(1);
    });

    test('Should map request to method: ', () => {
        const mockEvent = {
            path: '/api/users/options',
            httpMethod: 'GET',
            body: JSON.stringify({}),
        } as APIGatewayEvent;
        const mockCollection = {} as Collection;
        const usersService = new UsersService(
            mockEvent,
            mockCollection,
            requestingUser
        );
        expect(usersService.mapRequestToMethod()).toBe('getOptions');
    });
});

import * as aws from 'aws-sdk';
import { BaseService } from '../base-service';
import { APIGatewayEvent, APIGatewayProxyEvent } from 'aws-lambda';
import {
    Collection,
    DeleteWriteOpResultObject,
    InsertOneWriteOpResult,
    ObjectId,
} from 'mongodb';
import { User } from '../../declarations/user';
import {
    usersOptionsPipeline,
    QueryParams,
} from '../../aggregation-pipelines/user-options-pipeline';
import { ImageType, UsersRoutes } from '../../declarations/globalEnums';
import getUserByIdPipeline from '../../aggregation-pipelines/get-user-by-id';

export class UsersService extends BaseService<User> {
    static routeMap = {
        GET: {
            getUserById: UsersRoutes.getUserById,
            getUsers: UsersRoutes.getUsers,
            getOptions: UsersRoutes.getOptions, // * I didn't want to do this...see below
        },
        POST: {
            // refreshUser: UsersRoutes.refreshUser, //? I'm not sure why I made this...
            createUser: UsersRoutes.createUser,
        },
        PUT: {
            updateUser: UsersRoutes.updateUser,
        },
        DELETE: {
            deleteUser: UsersRoutes.deleteUser,
        },
    };
    body: unknown;
    event: APIGatewayEvent;
    requestingUser: User;

    constructor(event: APIGatewayProxyEvent, col: Collection, user: User) {
        super(event, col, UsersService.routeMap);
        this.body = JSON.parse(event.body);
        this.event = event;
        this.requestingUser = user;
    }

    async createUser(): Promise<InsertOneWriteOpResult<{ _id: ObjectId }>> {
        const body = super.handleFilter(
            this.body as {
                _id: string | ObjectId;
                companyId: string | ObjectId;
            }
        );

        return super.create((body as unknown) as User);
    }

    async deleteUser(): Promise<DeleteWriteOpResultObject> {
        const splitPath = this.event.path.split('/');
        const _id = new ObjectId(splitPath[splitPath.length - 1]);
        return super.delete(_id);
    }

    async getUserById(): Promise<User> {
        const id = this.paramMap['{id}']; //todo: might want to remove the curlies at some point...idk
        const projection: { [name: string]: unknown } = { password: 0 };
        return super.findById(id, { projection }); // * -> {projection: {password: 0}}
    }

    async getOptions(): Promise<User[]> {
        const filter = super.handleFilter(this.event.queryStringParameters);

        if ('searchAll' in filter) {
            delete filter.searchAll;
            return super.aggregate(
                usersOptionsPipeline(filter as QueryParams, true)
            );
        }

        filter.companyId = new ObjectId(this.requestingUser.companyId);
        return super.aggregate(
            usersOptionsPipeline(filter as QueryParams, false)
        );
    }

    async getUsers(): Promise<User[]> {
        const filter = super.handleFilter(this.event.queryStringParameters);
        const projection: { [name: string]: unknown } = { password: 0 };
        return super.get(filter, projection);
    }

    async refreshUser(): Promise<User> {
        return this.requestingUser;
    }

    async updateUser(): Promise<User> {
        const _id = super.getId(this.event.path);
        const body = this.body as { img: string };
        if ('img' in body) {
            body.img = await super.uploadImageToS3(
                body.img,
                ImageType.Profile,
                'user'
            );
        }
        await super.update({ _id }, body as User);
        const user = await super.aggregate(getUserByIdPipeline(_id));
        return user[0];
    }
}

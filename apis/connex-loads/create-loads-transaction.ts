import { APIGatewayProxyEvent } from 'aws-lambda';
import { Load } from 'declarations/load';
import { Collection, MongoClient, TransactionOptions } from 'mongodb';
import {
    BolTemplate,
    buildBols,
    fixLoad,
} from './loads-transactions/transaction-helpers';

export default async function createLoadTransaction(
    event: APIGatewayProxyEvent,
    client: MongoClient,
    loadsCol: Collection
) {
    const bolCollection = client.db('connex').collection('bols');
    const session = client.startSession();
    const transactionOptions: TransactionOptions = {
        readPreference: 'primary',
        readConcern: { level: 'local' },
        writeConcern: { w: 'majority' },
    };
    let newLoad: Load;
    let carrierId;

    await session.withTransaction(async () => {
        const load = fixLoad(JSON.parse(event.body));

        carrierId = load.carrierId; //Todo: see finally

        const { bols } = load;
        delete load.bols;
        load.bols = [];
        const bolDocs = buildBols((bols as unknown) as BolTemplate[], load);

        if (bolDocs.length) {
            await bolCollection.insertMany(bolDocs, { session });
        }
        const result = await loadsCol.insertOne(load, { session });
        newLoad = result.ops[0];
    }, transactionOptions);

    //todo: maybe? we might need to send an SNS to the receiver and carrier
    //todo: or some sort of push notification of the transaction was successful...
    await session.endSession();
    return { message: 'Load created!', newLoad };
}

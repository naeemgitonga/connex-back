import { BaseService } from '../base-service';
import { APIGatewayEvent, APIGatewayProxyEvent } from 'aws-lambda';
import {
    Collection,
    DeleteWriteOpResultObject,
    InsertOneWriteOpResult,
    ObjectId,
    UpdateWriteOpResult,
} from 'mongodb';

import { Load } from 'declarations/load';
import { User } from 'declarations/user';
import carrierLoads from '../../aggregation-pipelines/carrier-loads';
import getInbound from '../../aggregation-pipelines/inbound-loads/get-inbound';
import getOutbound from '../../aggregation-pipelines/outbound-loads/get-outbound';
import getCompleted from '../../aggregation-pipelines/completed-loads/get-completed';
import getUnsubmitted from '../../aggregation-pipelines/unsubmitted-loads/get-unsubmitted';
import driverLoads from '../../aggregation-pipelines/driver-loads';
import { LoadsRoutes } from '../../declarations/globalEnums';

export class LoadsService extends BaseService<Load> {
    static routeMap = {
        GET: {
            getLoadsPagination: LoadsRoutes.getLoadsPagination,
            getLoadById: LoadsRoutes.getLoadById,
            getLoads: LoadsRoutes.getLoads,
            getLoadsByCarrier: LoadsRoutes.getLoadsByCarrier,
            getLoadsByDriver: LoadsRoutes.getLoadsByDriver,
        },
        POST: {
            createLoad: LoadsRoutes.createLoad,
        },
        PUT: {
            updateLoad: LoadsRoutes.updateLoad,
        },
        DELETE: {
            deleteLoad: LoadsRoutes.deleteLoad,
        },
    };
    body: unknown;
    event: APIGatewayEvent;
    requestingUser: User;

    constructor(event: APIGatewayProxyEvent, col: Collection, user: User) {
        super(event, col, LoadsService.routeMap);
        this.body = JSON.parse(event.body);
        this.event = event;
        this.requestingUser = user;
    }

    async createLoad(): Promise<InsertOneWriteOpResult<{ _id: ObjectId }>> {
        const body = super.handleFilter(
            this.body as {
                [name: string]: unknown;
            }
        );

        return super.create((body as unknown) as Load);
    }

    async deleteLoad(): Promise<DeleteWriteOpResultObject> {
        const splitPath = this.event.path.split('/');
        const _id = new ObjectId(splitPath[splitPath.length - 1]);
        return super.delete(_id);
    }

    async getLoadById(id: ObjectId): Promise<Load> {
        return super.findById(id);
    }

    async getLoadsByDriver(): Promise<unknown> {
        const driverId = super.getId(this.event.path);
        const loads = await super.aggregate(driverLoads(driverId));
        return loads[0];
    }

    async getLoadsByCarrier(): Promise<unknown> {
        const splitPath = this.event.path.split('/');
        const carrierId = new ObjectId(splitPath[splitPath.length - 1]);
        const loads = await super.aggregate(carrierLoads(carrierId));
        return loads[0];
    }

    async getLoads(): Promise<Load[]> {
        const filter = super.handleFilter(this.event.queryStringParameters);
        return super.get(filter);
    }

    async updateLoad(): Promise<UpdateWriteOpResult> {
        const splitPath = this.event.path.split('/');
        const _id = new ObjectId(splitPath[splitPath.length - 1]);
        const body = super.handleFilter(
            this.body as { [name: string]: unknown }
        );
        return super.update({ _id }, (body as unknown) as Load);
    }

    async getLoadsPagination(): Promise<unknown> {
        const filter = super.handleFilter(this.event.queryStringParameters);
        let loads = [];
        const page = parseInt(filter.page as string);
        const type = filter.type;
        if (type === 'inbound') {
            loads = await super.aggregate(
                getInbound(this.requestingUser.companyId, page)
            );
        }

        if (type === 'outbound') {
            loads = await super.aggregate(
                getOutbound(this.requestingUser.companyId, page)
            );
        }

        if (type === 'completed') {
            loads = await super.aggregate(
                getCompleted(this.requestingUser.companyId, page)
            );
        }

        if (type === 'unsubmitted') {
            loads = await super.aggregate(
                getUnsubmitted(this.requestingUser.companyId, page)
            );
        }
        return loads[0];
    }
}

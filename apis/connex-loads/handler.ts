import {
    APIGatewayProxyEvent,
    APIGatewayProxyHandler,
    APIGatewayProxyResult,
    Context,
} from 'aws-lambda';
import { Collection, MongoClient } from 'mongodb';

import {
    Collections,
    TokenOptions,
    LoginErrors,
    ServerErrors,
} from '../../declarations/globalEnums';
import { response } from '../response';
import {
    connect,
    ConnectionResponse,
    retryConnection,
} from '../mongo-connection';
import { LoadsService } from './loads-service';
import AuthService from '../connex-auth/auth-service';
import createLoadTransaction from './create-loads-transaction';
import updateLoadTransaction from './update-loads-transaction';

export const loads: APIGatewayProxyHandler = async (
    event: APIGatewayProxyEvent,
    context: Context
): Promise<APIGatewayProxyResult> => {
    context.callbackWaitsForEmptyEventLoop = false;

    const { status, user } = AuthService.verifyToken(
        event,
        TokenOptions.Access
    );

    if (status !== 200) {
        return response(
            LoginErrors.Unauthorized,
            status,
            null,
            { 'Access-Control-Allow-Credentials': true },
            AuthService.getUnauthorizedHeaders()
        );
    }

    let collection: Collection;
    let client: MongoClient;
    let retries = 5;
    try {
        const { db, client: cl } = await connect();
        client = cl;
        collection = db.collection(Collections.Loads);
    } catch (e) {
        console.log(e);
        return response(e.errmsg, 500, e);
    }

    try {
        if (event.path === '/api/loads/create') {
            if (client === undefined) {
                client = await retryConnection(retries);
            }
            const res = await createLoadTransaction(event, client, collection);
            return response(res, 200);
        }
        if (event.path === '/api/loads/update') {
            if (client === undefined) {
                client = await retryConnection(retries);
            }
            const res = await updateLoadTransaction(event, client, collection);
            return response(res, 200);
        }

        const loadsService = new LoadsService(event, collection, user);
        const whichMethod = loadsService.mapRequestToMethod();
        const res = await loadsService[whichMethod]();
        return response(res, 200);
    } catch (e) {
        console.log(e);
        return response(ServerErrors.ItBroke, 500, e);
    }
};

import { Bol, OSD } from '../../../declarations/load';
import {
    ObjectId,
    BulkWriteUpdateOneOperation,
    MatchKeysAndValues,
} from 'mongodb';
//todo: DRY up some of these function's operations
export function createBulkBolWriteDoc(
    bol: BolTemplate
): BulkWriteUpdateOneOperation<Bol> {
    const _id = bol._id;
    delete bol._id;
    return {
        updateOne: {
            filter: { _id: _id as ObjectId },
            update: { $set: bol as MatchKeysAndValues<Bol> },
            upsert: true,
        },
    };
}

export function buildBols(
    bols: BolTemplate[],
    load: LoadTemplate,
    updating = false
): BolTemplate[] {
    return bols.map((b: BolTemplate) => {
        ['shipperId', 'receiverId'].forEach((prop: string) => {
            if (b[prop] !== undefined) {
                b[prop] = new ObjectId(b[prop]);
            }
        });

        ['dropDate'].forEach((prop: string) => {
            if (b[prop] !== undefined) {
                b[prop] = new Date(b[prop]);
            }
        });

        ['ready', 'deleted'].forEach((prop: string) => {
            if (b[prop] === 'false') {
                b[prop] = false;
            }

            if (b[prop] === 'true') {
                b[prop] = true;
            }
        });

        if (b.file?.lastModified) {
            b.file = {
                ...b.file,
                lastModified: Number(b.file.lastModified),
                lastModifiedDate: new Date(b.file.lastModifiedDate),
            };
        }

        if (b.file?.size !== undefined) {
            b.file.size = Number(b.file.size);
        }

        if (updating) {
            b._id = new ObjectId(b._id);
            load.bols.push(b._id);
            return b;
        }

        b._id = new ObjectId();
        load.bols.push(b._id);
        b.checkin = null;
        b.checkout = null;
        b.osd = {
            overages: [],
            shortages: [],
            damages: [],
        };

        return b;
    });
}

export function fixLoad(load: LoadTemplate, updating = false): LoadTemplate {
    ['shipperId', 'createdBy', 'carrierId'].forEach(prop => {
        if (load[prop] !== undefined) {
            load[prop] = new ObjectId(load[prop]);
        }
    });

    ['pUDate'].forEach(prop => {
        if (load[prop] !== undefined) {
            load[prop] = new Date(load[prop]);
        }
    });

    ['stops'].forEach(prop => {
        if (load[prop] !== undefined) {
            load[prop] = Number(load[prop]);
        }
    });

    ['ready', 'deleted'].forEach(prop => {
        if (load[prop] === 'false') {
            load[prop] = false;
        }

        if (load[prop] === 'true') {
            load[prop] = true;
        }
    });

    if (updating) return load;

    load.deleted = false;
    load.createdDate = new Date();
    load.checkin = null;
    load.checkout = null;
    return load;
}

//! these types are only for this file and used to get around typescript errors during testing
export type LoadTemplate = {
    _id?: string | ObjectId;
    deleted?: boolean;
    shipperId?: string; // * id of company moving the load
    createdBy?: string; // * person that created the load
    carrierId?: string;
    ready?: string;
    stops?: string | number;
    pUDate?: string;
    pUNum?: string;
    createdDate?: Date;
    checkin?: null | Date;
    checkout?: null | Date;
    bols?: ObjectId[];
};

type FileTemplate = {
    lastModified: string | number;
    lastModifiedDate: string | Date;
    name: number | string;
    size: string | number;
    type: string;
    webkitRelativePath: string;
};

export type BolTemplate = {
    _id?: string | ObjectId;
    file?: FileTemplate;
    shipperId?: string | ObjectId;
    driverId?: string | ObjectId;
    receiverId?: string | ObjectId;
    checkin?: null | Date;
    checkout?: null | Date;
    dropDate?: string | Date;
    osd?: OSD;
};

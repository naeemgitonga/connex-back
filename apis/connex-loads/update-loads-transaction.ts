import { APIGatewayProxyEvent } from 'aws-lambda';
import { Collection, MongoClient, TransactionOptions, ObjectId } from 'mongodb';
import {
    fixLoad,
    buildBols,
    BolTemplate,
    createBulkBolWriteDoc,
} from './loads-transactions/transaction-helpers';

export default async function updateLoadTransaction(
    event: APIGatewayProxyEvent,
    client: MongoClient,
    loadsCol: Collection
) {
    const bolCollection = client.db('connex').collection('bols');
    const session = client.startSession();
    const transactionOptions: TransactionOptions = {
        readPreference: 'primary',
        readConcern: { level: 'local' },
        writeConcern: { w: 'majority' },
    };
    let updatedLoad: unknown;
    let carrierId;

    await session.withTransaction(async () => {
        const load = fixLoad(JSON.parse(event.body), true);
        load._id = new ObjectId(load._id);
        carrierId = load.carrierId; //Todo: see finally

        const { bols } = load;
        delete load.bols;
        load.bols = [];
        const bolDocs = buildBols(
            (bols as unknown) as BolTemplate[],
            load,
            true
        );
        const bulkWriteOps = bolDocs.map(createBulkBolWriteDoc);

        await (bolDocs.length &&
            bolCollection.bulkWrite(bulkWriteOps, {
                session,
            }));

        const loadId = load._id;
        load.stops = bolDocs.length;
        delete load._id;
        const result = await loadsCol.updateOne(
            { _id: new ObjectId(loadId) },
            { $set: load },
            {
                session,
            }
        );
        updatedLoad = result;
    }, transactionOptions);

    //todo: maybe? we might need to send an SNS to the receiver and carrier
    //todo: or some sort of push notification of the transaction was successful...
    await session.endSession();
    return { message: 'Load updated!', updatedLoad };
}

import { APIGatewayEvent } from 'aws-lambda';
import { Collection, MongoClient, ObjectId } from 'mongodb';

import { LoadsService } from '../loads-service';
import { User } from '../../../declarations/user';

import updateLoadTransaction from '../update-loads-transaction';
import createLoadTransaction from '../create-loads-transaction';
import {
    LoadTemplate,
    fixLoad,
    buildBols,
    createBulkBolWriteDoc,
    BolTemplate,
} from '../loads-transactions/transaction-helpers';

const DB__CONNECTION = process.env.DB__CONNECTION;

describe('Loads service tests', () => {
    let collection: Collection;
    let bolsCollection: Collection;
    let client: MongoClient;
    const id = new ObjectId();
    const requestingUser = {
        companyId: new ObjectId('59cd4bf28c25a93904f6d401'),
        firstName: 'jaha naeem',
    } as User;
    beforeAll(async () => {
        try {
            client = await MongoClient.connect(DB__CONNECTION, {
                useNewUrlParser: true,
                useUnifiedTopology: true,
            });
            const db = client.db('connex');
            collection = db.collection('loads');
            bolsCollection = db.collection('bols');
        } catch (e) {
            console.log(e);
            throw new Error(e.errmsg);
        }
    });

    afterAll(async () => {
        try {
            await collection.deleteMany({ firstName: 'Test-user' });
            await bolsCollection.deleteMany({ 'file.lastModified': 234567 });
            await collection.deleteMany({ pUNum: 'test-transaction-1' });
            client.close();
        } catch (e) {
            console.log(e);
            throw new Error(e.errmsg);
        }
    });

    test('Should map request to method for singleton route: ', () => {
        const mockEvent = {
            path: '/api/loads/59cd4bf28c25a93904f6d401',
            httpMethod: 'GET',
            body: JSON.stringify({}),
        } as APIGatewayEvent;
        const mockCollection = {} as Collection;
        const loadsService = new LoadsService(
            mockEvent,
            mockCollection,
            requestingUser
        );
        expect(loadsService.mapRequestToMethod()).toBe('getLoadById');
    });

    test('Should map request to method with param in route: ', () => {
        const mockEvent = {
            path: '/api/loads',
            httpMethod: 'POST',
            body: JSON.stringify({}),
        } as APIGatewayEvent;
        const mockCollection = {} as Collection;
        const loadsService = new LoadsService(
            mockEvent,
            mockCollection,
            requestingUser
        );
        expect(loadsService.mapRequestToMethod()).toBe('createLoad');
    });

    test('Should map request to method: ', () => {
        const mockEvent = {
            path: '/api/loads/59cd4bf28c25a93904f6d401',
            httpMethod: 'PUT',
            body: JSON.stringify({}),
        } as APIGatewayEvent;
        const mockCollection = {} as Collection;
        const loadsService = new LoadsService(
            mockEvent,
            mockCollection,
            requestingUser
        );
        expect(loadsService.mapRequestToMethod()).toBe('updateLoad');
    });

    test('Should make a load: ', async () => {
        const mockEvent = {
            path: '/api/loads',
            httpMethod: 'POST',
            body: JSON.stringify({
                _id: id,
                ready: true,
                shipperId: new ObjectId(),
                createdBy: new ObjectId(),
                deleted: false,
                stops: 5,
                pUDate: new Date(),
                pUNum: 'seeaiew93fa',
            }),
        } as APIGatewayEvent;

        const loadsService = new LoadsService(
            mockEvent,
            collection,
            requestingUser
        );
        const whichMethod = loadsService.mapRequestToMethod();
        const { result } = await loadsService[whichMethod]();
        expect(result.n).toBe(1);
        expect(result.ok).toBe(1);
    });

    test('Should update a load: ', async () => {
        const mockEvent = {
            path: `/api/loads/${id}`,
            httpMethod: 'PUT',
            body: JSON.stringify({
                stops: 4,
            }),
        } as APIGatewayEvent;

        const loadsService = new LoadsService(
            mockEvent,
            collection,
            requestingUser
        );
        const whichMethod = loadsService.mapRequestToMethod();
        const { result } = await loadsService[whichMethod]();

        expect(result.n).toBe(1);
        expect(result.ok).toBe(1);
    });

    test('Should delete a load: ', async () => {
        const mockEvent = {
            path: `/api/loads/${id}`,
            httpMethod: 'DELETE',
            body: JSON.stringify({
                phone: '4046700060',
            }),
        } as APIGatewayEvent;

        const loadsService = new LoadsService(
            mockEvent,
            collection,
            requestingUser
        );
        const whichMethod = loadsService.mapRequestToMethod();
        const { result } = await loadsService[whichMethod]();

        expect(result.n).toBe(1);
        expect(result.ok).toBe(1);
    });

    test('Should get loads for pagination {outbound}: ', async () => {
        const mockEvent = ({
            path: '/api/loads/pagination',
            httpMethod: 'GET',
            body: JSON.stringify({}),
            //! I had to change to one because there is only 3
            //! outbound loads at the moment.
            queryStringParameters: { page: 1, type: 'outbound' },
        } as unknown) as APIGatewayEvent;

        const loadsService = new LoadsService(
            mockEvent,
            collection,
            requestingUser
        );
        const whichMethod = loadsService.mapRequestToMethod();
        const result = await loadsService[whichMethod]();
        expect('outboundTotal' in result).toBe(!!result.outbound.length);
        expect('outbound' in result).toBe(true);
        expect(Array.isArray(result.outbound)).toBe(true);
        expect(typeof result.outboundTotal).toBe(
            !!result.outbound.length ? 'number' : 'undefined'
        );
    });

    test('Should get loads for pagination {inbound}: ', async () => {
        const mockEvent = ({
            path: '/api/loads/pagination',
            httpMethod: 'GET',
            body: JSON.stringify({}),
            queryStringParameters: { page: 1, type: 'inbound' },
        } as unknown) as APIGatewayEvent;

        const loadsService = new LoadsService(
            mockEvent,
            collection,
            requestingUser
        );
        const whichMethod = loadsService.mapRequestToMethod();
        const result = await loadsService[whichMethod]();
        expect('inboundTotal' in result).toBe(!!result.inbound.length);
        expect('inbound' in result).toBe(true);
        expect(Array.isArray(result.inbound)).toBe(true);
        expect(typeof result.inboundTotal).toBe(
            !!result.inbound.length ? 'number' : 'undefined'
        );
    });

    test('Should get loads for pagination {unsubmitted}: ', async () => {
        const mockEvent = ({
            path: '/api/loads/pagination',
            httpMethod: 'GET',
            body: JSON.stringify({}),
            queryStringParameters: { page: 1, type: 'unsubmitted' },
        } as unknown) as APIGatewayEvent;

        const loadsService = new LoadsService(
            mockEvent,
            collection,
            requestingUser
        );
        const whichMethod = loadsService.mapRequestToMethod();
        const result = await loadsService[whichMethod]();
        expect('unsubmittedTotal' in result).toBe(!!result.unsubmitted.length);
        expect('unsubmitted' in result).toBe(true);
        expect(Array.isArray(result.unsubmitted)).toBe(true);
        expect(typeof result.unsubmittedTotal).toBe(
            !!result.unsubmitted.length ? 'number' : 'undefined'
        );
    });

    describe('Transactions test suite', (): void => {
        test('Should convert props to the proper type', (): void => {
            const load = {
                shipperId: '59cd4bf28c25a93904f6d401',
                createdBy: '59cd4bf28c25a93904f6d401',
                carrierId: '59cd4bf28c25a93904f6d401',
                pUDate: '2021-01-13T01:35:15.735Z',
                stops: '3',
                ready: 'true',
            };

            const fixedLoad: LoadTemplate = fixLoad(load as LoadTemplate);
            expect(typeof fixedLoad.stops).toBe('number');
            expect(typeof fixedLoad.shipperId).toBe('object');
            expect(typeof fixedLoad.carrierId).toBe('object');
            expect(typeof fixedLoad.pUDate).toBe('object');
            expect(typeof fixedLoad.ready).toBe('boolean');
            expect(typeof fixedLoad.deleted).toBe('boolean');
            expect(typeof fixedLoad.createdDate).toBe('object');
            expect(typeof fixedLoad.checkin).toBe('object');
            expect(typeof fixedLoad.checkout).toBe('object');
        });

        test('Should convert prop ready to proper type', (): void => {
            const load = {
                shipperId: '59cd4bf28c25a93904f6d401',
                createdBy: '59cd4bf28c25a93904f6d401',
                carrierId: '59cd4bf28c25a93904f6d401',
                pUDate: '2021-01-13T01:35:15.735Z',
                stops: '3',
                ready: 'false',
            };

            const fixedLoad: LoadTemplate = fixLoad(load as LoadTemplate);
            expect(typeof fixedLoad.ready).toBe('boolean');
        });

        test('Should properly format bols', (): void => {
            const load = {
                shipperId: '59cd4bf28c25a93904f6d401',
                createdBy: '59cd4bf28c25a93904f6d401',
                carrierId: '59cd4bf28c25a93904f6d401',
                pUDate: '2021-01-13T01:35:15.735Z',
                stops: '3',
                ready: 'false',
                bols: [],
            };
            const bolTemplate = [
                {
                    file: {
                        lastModified: '234567',
                        lastModifiedDate: '2021-01-13T01:35:15.735Z',
                        name: '',
                        size: '334',
                        type: '',
                        webkitRelativePath: '',
                    },
                    shipperId: '59cd4bf28c25a93904f6d401',
                    receiverId: '59cd4bf28c25a93904f6d401',
                    dropDate: '2021-01-13T01:35:15.735Z',
                },
            ];

            const bols = buildBols(bolTemplate, load);

            expect(typeof bols[0].shipperId).toBe('object');
            expect(typeof bols[0].receiverId).toBe('object');
            expect(typeof bols[0].dropDate).toBe('object');
            expect(typeof bols[0].file.lastModified).toBe('number');
            expect(typeof bols[0].file.lastModifiedDate).toBe('object');
        });

        test('Should perform create transaction', async (): Promise<void> => {
            const load = {
                shipperId: '59cd4bf28c25a93904f6d401',
                createdBy: '59cd4bf28c25a93904f6d401',
                carrierId: '59cd4bf28c25a93904f6d401',
                pUDate: '2021-01-13T01:35:15.735Z',
                pUNum: 'test-transaction-1',
                stops: '3',
                ready: 'false',
                bols: [],
            };
            const bolTemplate = [
                {
                    file: {
                        lastModified: '234567',
                        lastModifiedDate: '2021-01-13T01:35:15.735Z',
                        name: '',
                        size: '334',
                        type: '',
                        webkitRelativePath: '',
                    },
                    shipperId: '59cd4bf28c25a93904f6d401',
                    receiverId: '59cd4bf28c25a93904f6d401',
                    dropDate: '2021-01-13T01:35:15.735Z',
                },
            ];

            const mockEvent = {
                path: '/api/bols/59cd4bf28c25a93904f6d401',
                httpMethod: 'PUT',
                body: JSON.stringify({ ...load, bols: bolTemplate }),
            } as APIGatewayEvent;

            const transactionResponse = await createLoadTransaction(
                mockEvent,
                client,
                collection
            );
            expect(transactionResponse.message).toBe('Load created!');
            expect('newLoad' in transactionResponse).toBeTruthy();
        });

        test('Should perform create transaction without bol', async (): Promise<void> => {
            const load = {
                shipperId: '59cd4bf28c25a93904f6d401',
                createdBy: '59cd4bf28c25a93904f6d401',
                carrierId: '59cd4bf28c25a93904f6d401',
                pUDate: '2021-01-13T01:35:15.735Z',
                pUNum: 'test-transaction-1',
                stops: '3',
                ready: 'false',
                bols: [],
            };
            const bolTemplate = [];

            const mockEvent = {
                path: '/api/bols/59cd4bf28c25a93904f6d401',
                httpMethod: 'PUT',
                body: JSON.stringify({ ...load, bols: bolTemplate }),
            } as APIGatewayEvent;

            const transactionResponse = await createLoadTransaction(
                mockEvent,
                client,
                collection
            );
            expect(transactionResponse.message).toBe('Load created!');
            expect('newLoad' in transactionResponse).toBeTruthy();
        });

        test('Should create bulk write doc', (): void => {
            const bulkWriteDoc = createBulkBolWriteDoc({} as BolTemplate);
            expect('updateOne' in bulkWriteDoc).toBeTruthy();
        });

        test('Should properly update load/bols', async (): Promise<void> => {
            const load = {
                _id: '603bc5512c22236d8d0fde5f',
                pUDate: '2021-01-13T01:35:15.735Z',
                bols: [],
            };
            const bolTemplate = [
                {
                    dropDate: '2021-01-13T01:35:15.735Z',
                    _id: '603bc5512c22236d8d0fde5e',
                },
            ];

            const mockEvent = {
                path: '/api/bols/59cd4bf28c25a93904f6d401',
                httpMethod: 'PUT',
                body: JSON.stringify({ ...load, bols: bolTemplate }),
            } as APIGatewayEvent;
            const transactionResponse = await updateLoadTransaction(
                mockEvent,
                client,
                collection
            );

            expect(transactionResponse.message).toBe('Load updated!');
            expect('updatedLoad' in transactionResponse);
        });
    });
});

import { MongoClient, Db } from 'mongodb';

const DB__CONNECTION = process.env.DB__CONNECTION;
const options = {
    poolSize: 50,
    useNewUrlParser: true,
    useUnifiedTopology: false, //! locally this can be set to true but in the cloud will cause timeouts if not set to false
};
let db: Db;
let close: Function;

export interface ConnectionResponse {
    db: Db;
    close: Function;
    client: MongoClient;
}

export async function connect(): Promise<ConnectionResponse> {
    let client;
    if (!db) {
        client = await MongoClient.connect(DB__CONNECTION, options);
        db = client.db('connex');
        close = client.close;
    }

    return { db, close, client };
}

export async function retryConnection(retries: number): Promise<MongoClient> {
    if (retries === 0) throw new Error('Could not connect to database');
    const { client, close } = await connect();
    if (client !== undefined) return client;
    return retryConnection(retries - 1);
}

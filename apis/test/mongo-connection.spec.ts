import { connect } from '../mongo-connection';

describe('Connect function test', () => {
    it('Should create a new connection: ', async () => {
        let kill;
        try {
            const { db, close } = await connect();
            expect(db).toBeTruthy();
            kill = close;
        } catch (e) {
            console.log(e);
        }
        try {
            kill(); //todo: figure out why this fails
        } catch (e) {
            /* i don't want to see this log */
        }
    });
});

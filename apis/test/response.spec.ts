import { response } from '../response';

describe('Response test', () => {
    const log = console.log;
    afterEach(() => {
        console.log = log;
    });

    it('Body property should have type of string: ', async () => {
        const res = await response({ dog: 'bark' }, 200);
        expect(typeof res.body).toBe('string');
    });

    it('Should call console.log: ', async () => {
        console.log = jest.fn();
        await response({ dog: 'bark' }, 401, new Error('you unauthorized'));
        expect(console.log).toHaveBeenCalled();
    });

    it('Should have a header: ', async () => {
        const cookie = {
            'set-cookie': 'mywonderful=cookie',
        };
        const res = await response({ dog: 'bark' }, 200, null, cookie);
        expect(JSON.stringify(res.headers)).toEqual(JSON.stringify(cookie));
    });

    it('Should have a multivalue header: ', async () => {
        const multivalueheaders = {
            'set-cookie': ['mywonderful=cookie', 'myotherwonderful=cookietoo'],
        };
        const res = await response(
            { dog: 'bark' },
            200,
            null,
            {},
            multivalueheaders
        );
        expect(JSON.stringify(res.multiValueHeaders)).toEqual(
            JSON.stringify(multivalueheaders)
        );
    });
});

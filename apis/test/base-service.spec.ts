import { Collection } from 'mongodb';
import { APIGatewayProxyEvent } from 'aws-lambda';

import { BaseService } from '../base-service';
import { RouteMap } from '../../declarations/company';

describe('Base service tests', () => {
    let baseService;
    const event = {
        queryStringParameters: {
            id: 123,
            role: '4',
            page: '3',
            companyType: '1',
        },
    };
    beforeEach(() => {
        baseService = new BaseService<unknown>(
            (event as unknown) as APIGatewayProxyEvent,
            {} as Collection,
            { GET: { happy: '/asdf' } } as RouteMap
        );
    });

    it('Event param role should be of type number: ', async () => {
        const filter = baseService.handleFilter(event.queryStringParameters);
        expect('role' in filter).toBeTruthy();
        expect(filter.role).toBe(4);
        expect(typeof filter.role).toBe('number');
    });

    it('Event param page should be of type number: ', async () => {
        const filter = baseService.handleFilter(event.queryStringParameters);
        expect('page' in filter).toBeTruthy();
        expect(filter.page).toBe(3);
        expect(typeof filter.page).toBe('number');
    });

    it('Event param page should be of type number: ', async () => {
        const filter = baseService.handleFilter(event.queryStringParameters);
        expect('type' in filter).toBeTruthy();
        expect(filter.type).toBe(1);
        expect(typeof filter.type).toBe('number');
    });

    it('Event param objectIds should be of type object: ', async () => {
        const event = {
            queryStringParameters: {
                objectIds: '5ffe4e2ed105ebcca53857a6,6008de563bc7b17408673dd7',
            },
        };
        const filter = baseService.handleFilter(event.queryStringParameters);
        expect('objectIds' in filter).toBeTruthy();
        expect(typeof filter.objectIds[0]).toBe('object');
        expect(typeof filter.objectIds[1]).toBe('object');
    });

    it('Event param deleted should be of type boolean: ', async () => {
        const event = {
            queryStringParameters: {
                deleted: 'false',
            },
        };
        const filter = baseService.handleFilter(event.queryStringParameters);
        expect('deleted' in filter).toBeTruthy();
        expect(typeof filter.deleted).toBe('boolean');
    });

    it('Event param deleted should be of type boolean: ', async () => {
        const event = {
            queryStringParameters: {
                deleted: 'true',
            },
        };
        const filter = baseService.handleFilter(event.queryStringParameters);
        expect('deleted' in filter).toBeTruthy();
        expect(typeof filter.deleted).toBe('boolean');
    });
});

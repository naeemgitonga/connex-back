import { Collection, MongoClient, ObjectId } from 'mongodb';
import { genSaltSync, hashSync } from 'bcryptjs';

let client: MongoClient;
export let collection: Collection;
const stage = process.env.STAGE;
const DB__CONNECTION =
    stage === 'prod'
        ? process.env.DB__CONNECTION__PROD
        : process.env.DB__CONNECTION__STAGING;

export async function setUp(): Promise<void> {
    client = await MongoClient.connect(DB__CONNECTION, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
    });
    const db = client.db('connex');
    collection = db.collection('users');
    await makePerson();
}

export async function tearDown(): Promise<void> {
    await collection.deleteOne({ username: 'test@user.com' });
    await client.close();
}

async function makePerson(): Promise<void> {
    const salt = genSaltSync(12);
    const hash = hashSync('test', salt);
    await collection.insertOne({
        username: 'test@user.com',
        password: hash,
        firstName: 'test',
        lastName: 'user',
        phone: '4046700059',
        img: null,
        companyId: new ObjectId(),
        role: 1,
    });
}

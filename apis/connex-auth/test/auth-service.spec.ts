import { setUp, tearDown, collection } from './set-up-tear-down';
import AuthService from '../auth-service';
import { APIGatewayEvent } from 'aws-lambda';
import { User } from '../../../declarations/user';
import { TokenOptions } from '../../../declarations/globalEnums';
describe('Login test suite', () => {
    const log = console.log; //! see comment in should not authenticate
    beforeAll(async () => {
        try {
            await setUp();
        } catch (err) {
            console.log(err);
            throw new Error(err.errmsg);
        }
    });

    afterAll(async () => {
        try {
            await tearDown();
        } catch (err) {
            console.log(err);
            throw new Error(err.errmsg);
        }
    });

    afterEach(() => {
        console.log = log;
    });

    test('login: It should login a user:', async () => {
        const event = {
            body: JSON.stringify({
                username: 'test@user.com',
                password: 'test',
            }),
        } as APIGatewayEvent;
        const authService = new AuthService(event, collection);
        let { payload: user, error } = await authService.login();
        expect(error).toBe(null);
        expect(user).toBeTruthy();
    });

    test('login: It should not login user:', async () => {
        const event = {
            body: JSON.stringify({
                username: 'apple@user.com',
                password: 'test',
            }),
        } as APIGatewayEvent;
        const authService = new AuthService(event, collection);
        let { payload: user, error } = await authService.login();
        expect(error).toBe('User does not exist!');
        expect(user).toBe('User does not exist!');
    });

    test('secureToken: It should make token secure:', async () => {
        const cookie = AuthService.secureCookie(
            'prod',
            'ACCESS-TOKEN=a323iasdlfkiejafls;difjaief'
        );
        expect(cookie.includes('Secure')).toBeTruthy();
    });

    test('secureToken: It should not make token secure:', async () => {
        const cookie = AuthService.secureCookie(
            'localhost',
            'ACCESS-TOKEN=a323iasdlfkiejafls;difjaief'
        );
        expect(cookie.includes('Secure')).toBeFalsy();
    });

    describe('Verification tests', () => {
        test('Should verify the auth token is valid:', () => {
            const user = { username: 'jaha' } as User;
            const accessToken = AuthService.createToken(
                user,
                '1d',
                TokenOptions.Access
            );
            expect(AuthService.isValid(accessToken).status).toEqual(200);
        });

        test('Should deny token as invalid:', () => {
            const expiredToken =
                'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7Il9pZCI6IjU5Y2E3ZWU5MzAzNzBjMzQzY2RkMjM4MiIsInVzZXJuYW1lIjoiamFoYXBsYWNlQGhvdG1haWwuY29tIiwiZmlyc3ROYW1lIjoiSmFoYSBOYWVlbSIsImltZyI6IiIsImxhc3ROYW1lIjoiR2l0b25nYSIsInBob25lIjoiNDA0LTY3MC0wMDU5Iiwicm9sZSI6MSwiY29tcGFueUlEIjoiNTljZDRiZjI4YzI1YTkzOTA0ZjZkNDAxIiwiY29tcGFueUlkIjoiNTljZDRiZjI4YzI1YTkzOTA0ZjZkNDAxIiwiX192IjowfSwiaWF0IjoxNjA0MzU2MDc1LCJleHAiOjE2MDQzNjMyNzUsImlzcyI6ImNvbm5leGFwcC5jb20iLCJzdWIiOiJhY2Nlc3MifQ.4f_tk6ep_YwUXWVP9jcw926EE1h2nJULoEzdsLJYSBA';
            expect(AuthService.isValid('asfsdfasd').status).toEqual(401);
            expect(AuthService.isValid(expiredToken).status).toBe(403);
        });
    });

    describe('Authentication', () => {
        const cookieString = `
        _ga=GA1.1.1293491232.1604094524;
        _gid=GA1.1.2115882857.1604094524;
        _gat_gtag_UA_112911264_2=1;
        ACCESS-TOKEN=${AuthService.createToken(
            { username: 'jaha' } as User,
            AuthService.JWT_EXPIRY_ACCESS,
            TokenOptions.Access
        )};
        REFRESH-TOKEN=${AuthService.createToken(
            { username: 'jaha' } as User,
            AuthService.JWT_EXPIRY_REFRESH,
            TokenOptions.Refresh
        )};
        XSRF-TOKEN=${AuthService.createToken(
            { username: 'jaha' } as User,
            AuthService.JWT_EXPIRY_XSRF,
            TokenOptions.Xsrf
        )}
    `;
        test('Should have an access token:', () => {
            const cookies = AuthService.cookieParser(cookieString);
            const cookieNames = Object.keys(cookies);
            const hasAccessToken = cookieNames.includes('ACCESS-TOKEN');
            const hasXsrfToken = cookieNames.includes('XSRF-TOKEN');
            const hasRefreshToken = cookieNames.includes('REFRESH-TOKEN');

            expect(hasAccessToken).toBeTruthy();
            expect(hasXsrfToken).toBeTruthy();
            expect(hasRefreshToken).toBeTruthy();
        });

        test('Should not authenticate user:', () => {
            console.log = jest.fn(); //? do this so you won't have to see the log in the test
            expect(
                AuthService.authenticate(cookieString, TokenOptions.Issuer)
                    .status
            ).toEqual(403);
            expect(
                AuthService.authenticate('', TokenOptions.Access).status
            ).toBe(403);
        });
        test('Should authenticate user:', () => {
            const { status } = AuthService.authenticate(
                cookieString,
                TokenOptions.Access
            );
            expect(status).toEqual(200);
            expect(
                AuthService.authenticate(cookieString, TokenOptions.Refresh)
                    .status
            ).toEqual(200);
            expect(
                AuthService.authenticate(cookieString, TokenOptions.Xsrf).status
            ).toEqual(200);
        });
    });
});

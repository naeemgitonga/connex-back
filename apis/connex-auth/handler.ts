import {
    APIGatewayProxyEvent,
    APIGatewayProxyHandler,
    APIGatewayProxyResult,
    Context,
} from 'aws-lambda';
import { Collection } from 'mongodb';

// import KMSDecryption from '../kms-decryption';
import { connect } from '../mongo-connection';
import { response } from '../response';
import { Collections, ServerErrors } from '../../declarations/globalEnums';
import AuthService from './auth-service';

export const auth: APIGatewayProxyHandler = async (
    event: APIGatewayProxyEvent,
    context: Context
): Promise<APIGatewayProxyResult> => {
    context.callbackWaitsForEmptyEventLoop = false;
    let collection: Collection;
    // new KMSDecryption(process.env.AUTH_FUNCTION_NAME, process.env.ENCRYPT_TEST).decrypt();

    try {
        const { db } = await connect();
        collection = db.collection(Collections.Users);
    } catch (err) {
        console.log(err);
        return response(err.errmsg, 500, err);
    }

    try {
        const authService = new AuthService(event, collection);
        const whichMethod = authService.mapRequestToMethod();
        const {
            payload,
            headers,
            multiValueHeader,
            error,
            status,
        } = await authService[whichMethod]();
        return response(payload, status, error, headers, multiValueHeader);
    } catch (e) {
        console.log(e);
        return response(ServerErrors.ItBroke, 500, e);
    }
};

# Lessons Learned from designing an Auth Lambda on AWS

## Cookies locally
Problem: Use cookies locally, set-cookie not working (with Lambda).

The only way I was able to see cookies locally was to use the following setup:

Lambda allows you to do this one of two ways. We can use the regular headers property on
the response method 1. It would look something like this: 
```
{
    'Set-Cookie': 'XSRF-TOKEN=23adfjasdlfkasdf; HttpOnly; path=/; SameSite=Strict',
    'sEt-Cookie': 'REFRESH-TOKEN=2flfsdljfasdfesdfj; HttpOnly; path=/; SameSite=Strict',
    'seT-Cookie': 'ACCESS-TOKEN=998asdfliajei;sdf; HttpOnly; path=/; SameSite=Strict'
}
```
*this is not my prefered method **but** it works.

Notice that in method one I have different casings in the "set-cookie" properties. 
This is because we are not allowed to have duplicate keys. Your browser will correct the
casings and make them all lowercase this and that's the reason it works.

Method 2 is to use multivalue headers:
```
{
    'Set-Cookie': [
        'XSRF-TOKEN=23adfjasdlfkasdf; HttpOnly; path=/; SameSite=Strict',
        'REFRESH-TOKEN=2flfsdljfasdfesdfj; HttpOnly; path=/; SameSite=Strict',
        'ACCESS-TOKEN=998asdfliajeisdf; HttpOnly; path=/; SameSite=Strict'
    ]
}
```

`multiValueHeaders` is a property on the Lambda `APIGatewayProxyResult` object (interface, strut--depends on your language) that has your header value and takes a list of string values for your header.

Notice that the `HttpOnly` is set, the `path` is set, `SameSite` is set. We will not be setting `Secure`. This is `localhost` we don't need `Secure` for that. If we set `Secure` our browser will block the cookie and `set-cookie` will not work as expected.

Another item we purposefully left off is the `Domain`. You may find certain stackoverflow answers stating that you can use: `Domain=null`, `Domain=""`, or `Domain=http://127.0.0.1`. I found none of those worked--so I left it off.

Since I block cookies, I had to ensure that I enabled cookies on Chrome (my dev browser of choice).
I added `localhost` to "Sites that can always use cookies" this can be found in Settings > Privacy
and Security > Cookies and other site data.

For a production setup I created a method that will add the `Domain` and `Secure` properties.
import { Collection } from 'mongodb';
import { compareSync, genSaltSync, hashSync } from 'bcryptjs';
import { sign, verify } from 'jsonwebtoken';
import {
    APIGatewayEvent,
    APIGatewayProxyEvent,
    APIGatewayProxyResult,
} from 'aws-lambda';

import email from '../connex-communications/email';
import { BaseService } from '../base-service';
import { User } from '../../declarations/user';
import {
    DecodedToken,
    LoginCredentials,
    AuthResponse,
    TokenResponse,
    PasswordReset,
} from '../../declarations/login';
import {
    TokenOptions,
    LoginErrors,
    Cookies,
    TokenNames,
    JwtErrors,
    SuccessMessages,
    AuthRoutes,
} from '../../declarations/globalEnums';

export default class AuthService extends BaseService<User> {
    static routeMap = {
        GET: {
            logout: AuthRoutes.logout,
            refresh: AuthRoutes.refresh,
            passwordResetEmail: AuthRoutes.passwordResetEmail,
            validateResetPasswordToken: AuthRoutes.validateResetPasswordToken,
        },
        POST: {
            login: AuthRoutes.login,
            resetPassWordUnauthenticated:
                AuthRoutes.resetPassWordUnauthenticated,
        },
        PUT: {
            passwordResetByUserId: AuthRoutes.passwordResetByUserId,
        },
    };
    body: LoginCredentials | PasswordReset;
    event: APIGatewayEvent;
    user: User;
    salt = genSaltSync(12);

    static JWT_EXPIRY_ACCESS = process.env.JWT_EXPIRY_ACCESS;
    static JWT_EXPIRY_REFRESH = process.env.JWT_EXPIRY_REFRESH;
    static JWT_EXPIRY_XSRF = process.env.JWT_EXPIRY_XSRF;
    static JWT_SECRET = process.env.JWT_SECRET;
    static DOMAIN = process.env.DOMAIN;

    constructor(event: APIGatewayProxyEvent, col: Collection) {
        super(event, col, AuthService.routeMap);
        this.body = JSON.parse(event.body);
        this.event = event;
    }

    private hash(plainTextPassword): string {
        return hashSync(plainTextPassword, this.salt);
    }

    public static authenticate(
        cookieHeader: string,
        whichCookie: TokenOptions
    ): TokenResponse {
        const errorState = { status: 403 };
        let cookies;
        ///! if a user trys to access a resource and their
        //! request doesn't have any tokens this will blow
        //! up and we should jst return the default errorState
        try {
            cookies = AuthService.cookieParser(cookieHeader);
        } catch (e) {
            console.log('COOKIE PARSING ERR ', e);
            return errorState;
        }

        switch (whichCookie) {
            case TokenOptions.Access:
                return AuthService.isValid(cookies[TokenNames.Access]);
            case TokenOptions.Refresh:
                return AuthService.isValid(cookies[TokenNames.Refresh]);
            case TokenOptions.Xsrf:
                return AuthService.isValid(cookies[TokenNames.Xsrf]);
            default:
                return errorState;
        }
    }

    public static cookieParser(cookies: string): Cookies {
        const cookieArr = cookies.split(';');
        return cookieArr.reduce((obj: Cookies, cookieStr: string): Cookies => {
            const [cookieName, cookieValue] = cookieStr.split('=');
            return {
                ...obj,
                [cookieName.trim()]: cookieValue.trim(),
            };
        }, {});
    }

    static createToken(
        user: User,
        expiresIn: string,
        subject: TokenOptions
    ): string {
        return sign({ user }, AuthService.JWT_SECRET, {
            subject,
            expiresIn,
            algorithm: TokenOptions.Algorithm,
            issuer: TokenOptions.Issuer,
        });
    }

    public static getUnauthorizedHeaders(): APIGatewayProxyResult['multiValueHeaders'] {
        return {
            'Set-Cookie': [
                'REFRESH-TOKEN=""; path=/; expires=Thu, 01 Jan 1970 00:00:00 GMT',
                'XSRF-TOKEN=""; path=/; expires=Thu, 01 Jan 1970 00:00:00 GMT',
                'ACCESS-TOKEN=""; path=/; expires=Thu, 01 Jan 1970 00:00:00 GMT',
            ],
        };
    }

    static getUserTokens(user: User): AuthResponse['multiValueHeader'] {
        const xsrfTokenCookie = `XSRF-TOKEN=${AuthService.createToken(
            user,
            this.JWT_EXPIRY_XSRF,
            TokenOptions.Xsrf
        )}; HttpOnly; path=/; SameSite=Strict`;
        const refreshTokenCookie = `REFRESH-TOKEN=${AuthService.createToken(
            user,
            this.JWT_EXPIRY_REFRESH,
            TokenOptions.Refresh
        )}; HttpOnly; path=/; SameSite=Strict`;
        const accessTokenCookie = `ACCESS-TOKEN=${AuthService.createToken(
            user,
            this.JWT_EXPIRY_ACCESS,
            TokenOptions.Access
        )}; HttpOnly; path=/; SameSite=Strict`;

        return {
            'Set-Cookie': [
                AuthService.secureCookie(AuthService.DOMAIN, accessTokenCookie),
                AuthService.secureCookie(AuthService.DOMAIN, xsrfTokenCookie),
                AuthService.secureCookie(
                    AuthService.DOMAIN,
                    refreshTokenCookie
                ),
            ],
        };
    }

    static isValid(token: string): TokenResponse {
        try {
            const decodedToken = verify(
                token,
                AuthService.JWT_SECRET
            ) as DecodedToken;
            //* the following short circuit conditional is only to get around
            //* a typescript error: Property 'user' does not exist on type 'DecodedToken'

            return (
                'user' in decodedToken && {
                    status: 200,
                    user: decodedToken.user,
                }
            );
        } catch (e) {
            if (e.message === JwtErrors.Malformed)
                return { status: 401, user: null };
            //* come back to this and rework so that
            //* if token is expired you can do something else
            return { status: 403, user: null };
        }
    }

    async passwordResetEmail(): Promise<AuthResponse> {
        const { email: username } = super.handleFilter(
            this.event.queryStringParameters
        );
        const userResponse = await super.get({
            username: username,
        });
        const user = userResponse[0];
        const res = await email({
            id: user._id,
            email: username as string,
            token: AuthService.createToken(
                user as User,
                '15m',
                TokenOptions.PasswordReset
            ),
        });
        return {
            payload: res.message,
            error: null,
            status: res.status,
            headers: {},
            multiValueHeader: AuthService.getUserTokens(user),
        };
    }

    async passwordResetByUserId(): Promise<AuthResponse> {
        const _id = super.getId(this.event.path);
        const userResult = await super.get({ _id });
        const user = userResult[0];
        if (!user) {
            return {
                payload: LoginErrors.UserNotFound,
                error: LoginErrors.UserNotFound,
                status: 404,
                headers: {},
                multiValueHeader: AuthService.getUserTokens(user),
            };
        }

        let match;
        // * you have to do the following for union types in TS because the properties don't overlap on each interface. if you omit the if statement it will throw an error
        if ('oldPassword' in this.body) {
            match = compareSync(this.body.oldPassword, user.password);
        }

        if ('resetWithToken' in this.body) {
            match = this.body.resetWithToken;
        }

        if (!match) {
            return {
                payload: LoginErrors.IncorrectPassword,
                error: LoginErrors.IncorrectPassword,
                status: 409,
                headers: {},
                multiValueHeader: AuthService.getUserTokens(user),
            };
        }

        if ('confirmedPassword' in this.body) {
            await super.update(
                { _id },
                { password: this.hash(this.body.confirmedPassword) }
            );
        }

        return {
            payload: SuccessMessages.PasswordReset,
            error: null,
            status: 200,
            headers: {},
            multiValueHeader: AuthService.getUserTokens(user),
        };
    }

    validateResetPasswordToken(): TokenResponse {
        const params = super.handleFilter(this.event.queryStringParameters);
        return AuthService.isValid(params.token as string);
    }

    async login(): Promise<AuthResponse> {
        let username;
        if ('username' in this.body) {
            username = { username: this.body.username };
        }
        const userResult = await super.get(username);
        const user = userResult[0];
        if (!user) {
            return {
                payload: LoginErrors.UserNotFound,
                error: LoginErrors.UserNotFound,
                status: 404,
                headers: {},
                multiValueHeader: AuthService.getUnauthorizedHeaders(),
            };
        }

        let match;
        if ('password' in this.body) {
            match = compareSync(this.body.password, user.password);
        }
        if (!match) {
            return {
                payload: LoginErrors.Unauthorized,
                error: LoginErrors.Unauthorized,
                status: 401,
                headers: {},
                multiValueHeader: AuthService.getUnauthorizedHeaders(),
            };
        }

        delete user.password;
        this.user = user;
        return {
            payload: {
                ...user,
                name: `${user.firstName} ${user.lastName}`,
                connexId: user._id.toString().substring(18, 24),
                email: user.username,
            },
            error: null,
            status: 200,
            headers: {},
            multiValueHeader: AuthService.getUserTokens(user),
        };
    }

    async logout(): Promise<AuthResponse> {
        return {
            payload: 'You have been logged out. Come back soon!',
            error: null,
            status: 200,
            headers: {},
            multiValueHeader: AuthService.getUnauthorizedHeaders(),
        };
    }

    static secureCookie(domain: string, cookie: string): string {
        if (domain.includes('localhost')) return cookie;
        return `${cookie}; Secure; Domain=${domain}`;
    }

    public static verifyToken(
        event: APIGatewayProxyEvent,
        _token?: TokenOptions
    ): { status: number; user: User } {
        const cookies = event.headers.cookie || event.headers.Cookie;
        const authentication = this.authenticate(cookies, TokenOptions.Access);

        const user = 'user' in authentication ? authentication.user : null;
        const { status } = authentication;
        return { status, user };
    }
}

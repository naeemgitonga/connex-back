import { genSaltSync, hashSync } from 'bcryptjs';

export const salt = genSaltSync(12);
export const hash = (plainTextPassword: string) =>
    hashSync(plainTextPassword, salt);

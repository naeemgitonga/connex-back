import * as aws from 'aws-sdk';
import { APIGatewayProxyEvent } from 'aws-lambda';
import {
    Collection,
    DeleteWriteOpResultObject,
    InsertOneWriteOpResult,
    ObjectId,
    UpdateWriteOpResult,
} from 'mongodb';
import { ParamMap, RouteMap } from '../declarations/company';
type In = {
    $in: ObjectId[];
};

export class BaseService<T> {
    path: string;
    method: string;
    collection: Collection;
    public routeMap: RouteMap;
    regex = RegExp(/({id}|{adminId}|{type})/gm);
    //* this may help us if we have routes with multiple route params
    //* in your service you can refer to this using super.paramMap
    //* as a lookup table
    paramMap: ParamMap = {};

    constructor(
        event: APIGatewayProxyEvent,
        col: Collection,
        routeMap: RouteMap
    ) {
        this.path = event.path;
        this.method = event.httpMethod;
        this.collection = col;
        this.routeMap = routeMap;
    }

    mapRequestToMethod(): string {
        const group = this.routeMap[this.method];
        for (const [routeName, routePath] of Object.entries(group)) {
            if (routePath === this.path) return routeName;
            const brokenTablePath = (routePath as string).split('/');
            const brokenEventPath = this.path.split('/');
            const isPath = this.compareRoutePaths(
                brokenTablePath,
                brokenEventPath
            );
            if (isPath) return routeName; //TODO: fix when route doesn't exist it's not throwing the following error
        }
        throw new Error(
            `We don't have a method to handle this request: ${this.path}`
        );
    }

    private compareRoutePaths(tp: string[], p2: string[]): boolean {
        if (p2.length !== tp.length) {
            return false;
        }

        let isPath = true;
        tp.forEach((e, i) => {
            if (!isPath) return;
            if (this.regex.test(e)) {
                try {
                    // * found out that there was a bug with
                    // * this. our other route tables had the
                    // * routes with the route parameters not
                    // * listed first in the object and if it
                    // * encountered one without a
                    // * single String of 12 bytes or a string of 24 hex characters
                    // * it would blow up the program.
                    // * here we test it first and if it blows up we
                    // * know we don't have a match
                    new ObjectId(p2[i]);
                } catch (e) {
                    isPath = false;
                    return;
                }
                this.paramMap = {
                    ...this.paramMap,
                    [e]: new ObjectId(p2[i]),
                };
                isPath = true;
                return;
            }

            if (e === p2[i]) {
                isPath = true;
                return;
            }

            isPath = false;
        });
        return isPath;
    }

    async get(
        filter: { [name: string]: unknown },
        projection = {}
    ): Promise<T[]> {
        const result = await this.collection.find(filter, projection).toArray();
        return result;
    }

    async findById(id: ObjectId, projection = {}): Promise<T> {
        const document = await this.collection.findOne({ _id: id }, projection);
        return document;
    }

    async create(doc: T): Promise<InsertOneWriteOpResult<{ _id: any }>> {
        const result = await this.collection.insertOne(doc);
        return result;
    }

    async delete(id: ObjectId): Promise<DeleteWriteOpResultObject> {
        const result = await this.collection.deleteOne({ _id: id });
        return result;
    }

    async update(
        filter: { _id: ObjectId | In },
        update: unknown,
        many = false
    ): Promise<UpdateWriteOpResult> {
        let result;
        if (many) {
            result = await this.collection.updateMany(filter, {
                $set: update,
            });
        } else {
            result = await this.collection.updateOne(filter, {
                $set: update,
            });
        }
        return result;
    }

    async aggregate(pipeline: object[]): Promise<T[]> {
        const result = await this.collection.aggregate(pipeline).toArray();
        return result;
    }

    //todo: handleFilter should take in a type object and return a type object
    //todo: make this more efficient. right now it will run each forEach...
    //todo regardless of wethere the filter has one of those props...
    //todo we should only run one of the forEach if a prop in the list is defined
    /**
     *
     * mainly used for query parameters;
     * we have added checkin/checkout to help with the body
     * because dates come in JSON as a string. we do not need
     * to add 'sealIntact' to the boolean section because it
     * is JSON and will retain it's type.
     */
    handleFilter(filter: {
        [name: string]: unknown;
    }): { [name: string]: unknown } {
        // * to convert string to integer add prop name in array below
        ['role', 'page', 'companyType'].forEach(key => {
            if (filter[key] !== undefined) {
                filter[key] = parseInt(filter[key] as string);

                if (key === 'companyType') {
                    filter['type'] = filter[key];
                    delete filter.companyType;
                }
            }
        });

        //* seal intact does not need to go here because it is part of the body
        //* these are
        ['deleted', 'getAllLoads', 'searchAll', 'ready'].forEach(key => {
            if (filter[key] === 'false') {
                filter[key] = false;
            } else if (filter[key] === 'true') {
                filter[key] = true;
            }
        });

        ['checkin', 'checkout', 'pUDate'].forEach(key => {
            if (filter[key] === null) {
                return;
            }

            if (filter[key] !== undefined) {
                filter[key] = new Date(filter[key] as string);
            }
        });

        ['_id', 'shipperId', 'createdBy', 'companyId'].forEach(key => {
            if (filter[key] !== undefined) {
                filter[key] = new ObjectId(filter[key] as string);
            }
        });

        if (filter.objectIds) {
            filter.objectIds = (filter.objectIds as string)
                .split(',')
                .map(id => new ObjectId(id));
        }

        return filter;
    }

    getId(path: string): ObjectId {
        const splitPath = path.split('/');
        return new ObjectId(splitPath[splitPath.length - 1]);
    }

    async uploadImageToS3(
        img: string,
        picturePurpose: string,
        bucket: string
    ): Promise<string> {
        const s3 = new aws.S3();
        const base64Data = Buffer.from(
            img.replace(/^data:image\/\w+;base64,/, ''),
            'base64'
        );
        const type = img.split(';')[0].split('/')[1];

        // * With this setup, each time your user uploads an image, will be overwritten.
        // * To prevent this, use a different Key each time.
        // * This won't be needed if they're uploading their avatar, hence the filename, userAvatar.js.
        const params = {
            Bucket: `connex-imgs/${bucket}-pictures`,
            Key: `${this.getId(this.path)}.${picturePurpose}.${type}`, // type is not required
            Body: base64Data,
            ACL: 'public-read',
            ContentEncoding: 'base64', //! required
            ContentType: `image/${type}`, //! required
        };

        let location = '';
        let key = '';
        const { Location, Key } = await s3.upload(params).promise();
        location = Location;
        key = Key;

        return location;
    }
}

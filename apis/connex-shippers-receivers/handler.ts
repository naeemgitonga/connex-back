import { APIGatewayProxyHandler } from 'aws-lambda';

export const shippersReceivers: APIGatewayProxyHandler = async (event, _context) => {
    return Promise.resolve({
		body: 'getting shippers-receivers',
		headers :{},
		isBase64Encoded :false,
		statusCode: 200
	});
};
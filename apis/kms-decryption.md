```
import { config, KMS } from 'aws-sdk';
// Todo: we should be using encrypted keys on our lambda functions
export default class KMSDecryption {
    decrypted: string;
    functionName: string;
    encryptedKey: string;

    constructor(functionName, encryptedKey) {
        this.functionName = functionName;
        this.encryptedKey = encryptedKey;
        config.update({ region: 'us-east-1' });
    }

    async decrypt() {
        if (!this.decrypted) {
            // ? Do these comments matter here with OOP?
            // Decrypt code should run once and variables stored outside of the
            // function handler so that these are decrypted once per container
            const kms = new KMS();
            try {
                const req = {
                    CiphertextBlob: Buffer.from(this.encryptedKey, 'base64'),
                    EncryptionContext: {
                        LambdaFunctionName: this.functionName,
                    },
                };
                const data = await kms.decrypt(req).promise();
                this.decrypted = data.Plaintext.toString('ascii');
                return this.decrypted;
            } catch (err) {
                console.log('Decrypt error:', err);
                throw err;
            }
        }
        console.log(this.decrypted);
        return this.decrypted;
    }
}
```

import { APIGatewayEvent } from 'aws-lambda';
import { Collection, MongoClient, ObjectId } from 'mongodb';

import { BolsService } from '../bols-service';
import { User } from '../../../declarations/user';
import { Bol } from '../../../declarations/load';

const DB__CONNECTION = process.env.DB__CONNECTION;

describe('Users service tests', () => {
    let collection: Collection;
    let client: MongoClient;
    const _id = new ObjectId();
    const requestingUser = {
        companyId: new ObjectId('59cd4bf28c25a93904f6d401'),
        firstName: 'jaha naeem',
    } as User;
    beforeAll(async () => {
        try {
            client = await MongoClient.connect(DB__CONNECTION, {
                useNewUrlParser: true,
                useUnifiedTopology: true,
            });
            const db = client.db('connex');
            collection = db.collection('bols');
            await collection.insertOne({
                _id,
                shipperId: new ObjectId(),
                file: null,
                checkout: null,
                checkin: null,
                dropDate: new Date(),
                osd: {
                    overages: [],
                    shortages: [],
                    damages: [],
                },
            } as Bol);
        } catch (e) {
            console.log(e);
            throw new Error(e.errmsg);
        }
    });

    afterAll(async () => {
        try {
            await collection.deleteOne({ _id });
            client.close();
        } catch (e) {
            console.log(e);
            throw new Error(e.errmsg);
        }
    });

    test('Should map request to method for singleton route: ', () => {
        const mockEvent = {
            path: '/api/bols/59cd4bf28c25a93904f6d401',
            httpMethod: 'PUT',
            body: JSON.stringify({}),
        } as APIGatewayEvent;
        const mockCollection = {} as Collection;
        const loadsService = new BolsService(
            mockEvent,
            mockCollection,
            requestingUser
        );
        expect(loadsService.mapRequestToMethod()).toBe('updateBol');
    });

    test('Should get loads for pagination {completed}: ', async () => {
        const mockEvent = ({
            path: '/api/bols/pagination',
            httpMethod: 'GET',
            body: JSON.stringify({}),
            queryStringParameters: { page: 1, type: 'completed' },
        } as unknown) as APIGatewayEvent;

        const bolsService = new BolsService(
            mockEvent,
            collection,
            requestingUser
        );
        const whichMethod = bolsService.mapRequestToMethod();
        const result = await bolsService[whichMethod]();
        expect('completed' in result).toBe(true);

        expect('completedTotal' in result).toBe(!!result.completed.length);
        expect(Array.isArray(result.completed)).toBe(true);
        expect(typeof result.completedTotal).toBe(
            !!result.completed.length ? 'number' : 'undefined'
        );
    });

    test('Should update bols with driverId: ', async () => {
        const mockEvent = ({
            path: '/api/bols/driver',
            httpMethod: 'PUT',
            body: JSON.stringify({ driverId: null }),
            queryStringParameters: {
                objectIds: '5ffe4e2ed105ebcca53857a6,6008de563bc7b17408673dd7',
            },
        } as unknown) as APIGatewayEvent;

        const bolsService = new BolsService(
            mockEvent,
            collection,
            requestingUser
        );
        const whichMethod = bolsService.mapRequestToMethod();
        await bolsService[whichMethod]();
        mockEvent.body = JSON.stringify({
            driverId: '59cd4ba98c25a93904f6d3ff',
        });
        mockEvent.queryStringParameters = {
            objectIds: '5ffe4e2ed105ebcca53857a6,6008de563bc7b17408673dd7',
        };
        const { result } = await bolsService[whichMethod]();
        expect(result.nModified).toBe(2);
    });

    test('Should update bol checkin date/time: ', async () => {
        const mockEvent = ({
            path: '/api/bols/603e32c3b89eec4b48383e23',
            httpMethod: 'PUT',
            body: JSON.stringify({ checkin: new Date() }),
        } as unknown) as APIGatewayEvent;

        const bolsService1 = new BolsService(
            mockEvent,
            collection,
            requestingUser
        );
        const whichMethod = bolsService1.mapRequestToMethod();
        await bolsService1[whichMethod]();
        const { result: firstResult } = await bolsService1[whichMethod]();
        expect(firstResult.nModified).toBe(0);

        mockEvent.body = JSON.stringify({ checkin: null });
        const bolsService2 = new BolsService(
            mockEvent,
            collection,
            requestingUser
        );
        const { result: secondResult } = await bolsService2[whichMethod]();
        expect(secondResult.nModified).toBe(1);
    });

    test('Should update bol completed: ', async () => {
        const mockSns = {
            publish: _payload => ({
                promise: () =>
                    Promise.resolve({
                        status: 200,
                        message: 'message being sent',
                    }),
            }),
        };
        const mockEvent = ({
            path: '/api/bols/603e32c3b89eec4b48383e23',
            httpMethod: 'PUT',
            body: JSON.stringify({ completed: true }),
        } as unknown) as APIGatewayEvent;

        const bolsService1 = new BolsService(
            mockEvent,
            collection,
            requestingUser
        );
        const whichMethod = bolsService1.mapRequestToMethod();
        const result = await bolsService1[whichMethod](mockSns);
        expect('smsResponse' in result).toBeTruthy();
        expect(result.smsResponse.status).toBe(200);
        expect(result.smsResponse.message).toBe('message being sent');

        mockEvent.body = JSON.stringify({ completed: false });
        const bolsService2 = new BolsService(
            mockEvent,
            collection,
            requestingUser
        );
        const { result: secondResult } = await bolsService2[whichMethod](
            mockSns
        );
        expect(secondResult.nModified).toBe(1);
    });
});

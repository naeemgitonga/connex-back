import { APIGatewayEvent, APIGatewayProxyEvent } from 'aws-lambda';
import { SNS } from 'aws-sdk';
import {
    Collection,
    DeleteWriteOpResultObject,
    ObjectId,
    UpdateWriteOpResult,
} from 'mongodb';

import { BaseService } from '../base-service';
import carrierLoads from '../../aggregation-pipelines/carrier-loads';
import { User } from 'declarations/user';
import { Bol } from 'declarations/load';
import { sms } from '../connex-communications/sms';
import getCompleted from '../../aggregation-pipelines/completed-loads/get-completed';
import getDriverBols from '../../aggregation-pipelines/get-driver-bols-pipeline';
import { BolsRoute } from '../../declarations/globalEnums';

export class BolsService extends BaseService<Bol> {
    static routeMap = {
        GET: {
            getLoadsForCarrier: BolsRoute.getLoadsForCarrier,
            getPaginatedCompleted: BolsRoute.getPaginatedCompleted,
            getBolsForDriver: BolsRoute.getBolsForDriver,
        },
        POST: {},
        PUT: {
            updateBol: BolsRoute.updateBol,
            updateBolsDrivers: BolsRoute.updateBolsDrivers,
        },
        DELETE: {
            deleteBol: BolsRoute.deleteBol,
        },
    };
    body: unknown;
    event: APIGatewayEvent;
    requestingUser: User;

    constructor(event: APIGatewayProxyEvent, col: Collection, user: User) {
        super(event, col, BolsService.routeMap);
        this.body = JSON.parse(event.body);
        this.event = event;
        this.requestingUser = user;
    }

    async deleteBol(): Promise<DeleteWriteOpResultObject> {
        const splitPath = this.event.path.split('/');
        const _id = new ObjectId(splitPath[splitPath.length - 1]);
        return super.delete(_id);
    }

    async updateBol(mockSns?: SNS): Promise<UpdateWriteOpResult> {
        const splitPath = this.event.path.split('/');
        const _id = new ObjectId(splitPath[splitPath.length - 1]);
        const body: object = super.handleFilter(
            this.body as { [name: string]: unknown }
        );
        let result;

        if ('completed' in (body as Bol)) {
            result = await super.update({ _id }, body as Bol);
            const smsResponse = await sms(
                {
                    Message: `CONNEX APP \nYou're load .... has been completed \nThank you for using Connex!`,
                    PhoneNumber: '+14046700059',
                },
                mockSns
            );
            result.smsResponse = smsResponse;
            return result;
        }
        return super.update({ _id }, body as Bol);
    }

    async getLoadsForCarrier(): Promise<Bol[]> {
        const splitPath = this.event.path.split('/');
        const _id = new ObjectId(splitPath[splitPath.length - 1]);
        return super.aggregate(carrierLoads(_id));
    }

    async getPaginatedCompleted(): Promise<unknown> {
        const filter = super.handleFilter(this.event.queryStringParameters);
        const page = parseInt(filter.page as string);
        const loads = await super.aggregate(
            getCompleted(this.requestingUser.companyId, page)
        );
        return loads[0];
    }

    async updateBolsDrivers(): Promise<UpdateWriteOpResult> {
        const filter = super.handleFilter(this.event.queryStringParameters);
        const result = await super.update(
            { _id: { $in: filter.objectIds as ObjectId[] } },
            {
                driverId: new ObjectId(
                    (this.body as { driverId: string }).driverId
                ),
            },
            true
        );
        return result;
    }

    async getBolsForDriver(): Promise<{ receiving: Bol[]; shipping: Bol[] }> {
        const driverId = super.getId(this.event.path);
        const companyId = this.requestingUser.companyId;

        const bols = await super.aggregate(
            getDriverBols(driverId, new ObjectId(companyId))
        );

        const shipping = this.filterShippingOrReceiving(
            bols,
            (companyId as unknown) as string,
            true
        );
        const receiving = this.filterShippingOrReceiving(
            bols,
            (companyId as unknown) as string
        );

        return {
            receiving,
            shipping,
        };
    }

    private filterShippingOrReceiving(
        bols: Bol[],
        companyId: string,
        shipping = false
    ): Bol[] {
        return bols.filter(b => {
            console.log(typeof b.shipperId);
            if (shipping) {
                return (
                    ((b.shipperId as unknown) as string).toString() ===
                    ((companyId as unknown) as string).toString()
                );
            }
            return (
                ((b.receiverId as unknown) as string).toString() ===
                ((companyId as unknown) as string).toString()
            );
        });
    }
}

import {
    APIGatewayProxyEvent,
    APIGatewayProxyHandler,
    APIGatewayProxyResult,
    Context,
} from 'aws-lambda';
import { Collection } from 'mongodb';

import {
    Collections,
    TokenOptions,
    LoginErrors,
    ServerErrors,
} from '../../declarations/globalEnums';
import { response } from '../response';
import { connect } from '../mongo-connection';
import { BolsService } from './bols-service';
import AuthService from '../connex-auth/auth-service';

export const bols: APIGatewayProxyHandler = async (
    event: APIGatewayProxyEvent,
    context: Context
): Promise<APIGatewayProxyResult> => {
    context.callbackWaitsForEmptyEventLoop = false;

    const { status, user } = AuthService.verifyToken(
        event,
        TokenOptions.Access
    );

    if (status !== 200)
        return response(
            LoginErrors.Unauthorized,
            status,
            null,
            { 'Access-Control-Allow-Credentials': true },
            AuthService.getUnauthorizedHeaders()
        );

    let collection: Collection;

    try {
        const connection = await connect();
        collection = connection.db.collection(Collections.Bols);
    } catch (e) {
        console.log(e);
        return response(e.errmsg, 500, e);
    }

    try {
        const bolsService = new BolsService(event, collection, user);
        const whichMethod = bolsService.mapRequestToMethod();
        const res = await bolsService[whichMethod]();
        return response(res, 200);
    } catch (e) {
        console.log(e);
        return response(ServerErrors.ItBroke, 500, e);
    }
};

import { APIGatewayProxyResult as result } from 'aws-lambda';

export const response = (
    body: unknown,
    statusCode: number,
    err: Error = null,
    headers: result['headers'] = {},
    multiValueHeaders = {}
): Promise<result> => {
    if (err) console.log(err);
    
    return Promise.resolve({
        body: JSON.stringify(body),
        headers,
        isBase64Encoded: false,
        statusCode,
        multiValueHeaders
    });
};

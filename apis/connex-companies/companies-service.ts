import {
    Collection,
    DeleteWriteOpResultObject,
    InsertOneWriteOpResult,
    ObjectId,
} from 'mongodb';
import { APIGatewayEvent, APIGatewayProxyEvent } from 'aws-lambda';
//todo: find out why your lambdas don't have versions in the gui
import { BaseService } from '../base-service';
import { User } from '../../declarations/user';
import { Company } from '../../declarations/company';
import { CompaniesRoutes, ImageType } from '../../declarations/globalEnums';
import { makeCompanyPipeline } from '../../aggregation-pipelines/companies-pipeline';
import { getAllLoadsPipeline } from '../../aggregation-pipelines/get-all-loads-pipeline';

export class CompaniesService extends BaseService<Company> {
    static routeMap = {
        GET: {
            getCompanies: CompaniesRoutes.getCompanies,
            getCompany: CompaniesRoutes.getCompany,
            getCompanyLoadsUsers: CompaniesRoutes.getCompanyLoadsUsers,
            getCompaniesByIds: CompaniesRoutes.getCompaniesByIds,
            //Todo: remove this from here and put it in the loads service
            getAllLoads: CompaniesRoutes.getAllLoads,
        },
        POST: {
            createCompany: CompaniesRoutes.createCompany,
            companies: CompaniesRoutes.companies,
        },
        PUT: {
            updateCompany: CompaniesRoutes.updateCompany,
        },
        DELETE: {
            deleteCompany: CompaniesRoutes.deleteCompany,
        },
    };
    body: unknown;
    event: APIGatewayEvent;
    companyId: ObjectId;

    constructor(event: APIGatewayProxyEvent, col: Collection, user: User) {
        super(event, col, CompaniesService.routeMap);
        this.body = JSON.parse(event.body);
        this.event = event;
        this.companyId = new ObjectId(user.companyId);
    }

    async getCompany(): Promise<Company> {
        return super.findById(this.companyId);
    }

    async getCompanies(): Promise<Company[]> {
        const filter = super.handleFilter(this.event.queryStringParameters);
        filter.name = { $regex: filter.name, $options: 'i' };
        return super.get(filter);
    }

    async getCompaniesByIds(): Promise<Company[]> {
        const filter = super.handleFilter(this.event.queryStringParameters);
        return super.get({ _id: { $in: filter.objectIds } });
    }

    async createCompany(): Promise<InsertOneWriteOpResult<{ _id: ObjectId }>> {
        return super.create(this.body as Company);
    }

    async updateCompany(): Promise<Company> {
        const _id = super.getId(this.event.path);
        const body = this.body as { img: string };
        if ('img' in body) {
            body.img = await super.uploadImageToS3(
                body.img,
                ImageType.Profile,
                'company'
            );
        }
        await super.update({ _id }, this.body as Company);
        const company = await super.aggregate(makeCompanyPipeline(_id, false));
        return company[0];
    }

    async deleteCompany(): Promise<DeleteWriteOpResultObject> {
        const _id = super.getId(this.event.path);
        return super.delete(_id);
    }

    async getCompanyLoadsUsers(): Promise<Company> {
        const filter = super.handleFilter(this.event.queryStringParameters);
        const company = await super.aggregate(
            makeCompanyPipeline(this.companyId, filter.getAllLoads as boolean)
        );
        return company[0];
    }

    //? why put this here...
    //* because i was lazy and didn't feel like
    //* rethinking the agg pipeline
    //todo: for tomorrow move this back to loads
    //todo: rewrite the agg pipeline to first get
    //todo: outbound, unsubmitted, completed, and last
    //todo: bols
    async getAllLoads(): Promise<unknown> {
        const loads = await super.aggregate(
            getAllLoadsPipeline(this.companyId)
        );
        return loads[0];
    }
}

import { APIGatewayEvent } from 'aws-lambda';
import { Collection, MongoClient, ObjectId } from 'mongodb';
import { CompanyType } from '../../../declarations/globalEnums';
import { User } from '../../../declarations/user';
import { CompaniesService } from '../companies-service';

const DB__CONNECTION = process.env.DB__CONNECTION;

describe('Companies service tests', () => {
    let collection: Collection;
    let client: MongoClient;
    const id = new ObjectId();
    const user = { companyId: id } as User;
    beforeAll(async () => {
        try {
            client = await MongoClient.connect(DB__CONNECTION, {
                useNewUrlParser: true,
                useUnifiedTopology: true,
            });
            const db = client.db('connex');
            collection = db.collection('companies');
        } catch (e) {
            console.log(e);
            throw new Error(e.errmsg);
        }
    });

    afterAll(async () => {
        try {
            await collection.deleteMany({ name: 'Test' });
            client.close();
        } catch (e) {
            console.log(e);
            throw new Error(e.errmsg);
        }
    });

    test('Should map request to method for singleton route: ', () => {
        const mockEvent = {
            path: '/api/companies/59cd4bf28c25a93904f6d401',
            httpMethod: 'GET',
            body: JSON.stringify({}),
        } as APIGatewayEvent;
        const mockCollection = {} as Collection;
        const companiesService = new CompaniesService(
            mockEvent,
            mockCollection,
            user
        );
        expect(companiesService.mapRequestToMethod()).toBe('getCompany');
    });

    test('Should map request to method with param in route: ', () => {
        const mockEvent = {
            path: '/api/companies/59cd4bf28c25a93904f6d401',
            httpMethod: 'POST',
            body: JSON.stringify({}),
        } as APIGatewayEvent;
        const mockCollection = {} as Collection;
        const companiesService = new CompaniesService(
            mockEvent,
            mockCollection,
            user
        );
        expect(companiesService.mapRequestToMethod()).toBe('companies');
    });

    test('Should map request to method: ', () => {
        const mockEvent = {
            path: '/api/companies/users/loads',
            httpMethod: 'GET',
            body: JSON.stringify({}),
        } as APIGatewayEvent;
        const mockCollection = {} as Collection;
        const companiesService = new CompaniesService(
            mockEvent,
            mockCollection,
            user
        );
        expect(companiesService.mapRequestToMethod()).toBe(
            'getCompanyLoadsUsers'
        );
    });

    test('Should NOT map request to method with params in route: ', () => {
        const mockEvent = {
            path:
                '/api/companies/59cd4bf28c25a93904f6d401/admin/59cd4bf28c25a93904f6d401/users',
            httpMethod: 'GET',
            body: JSON.stringify({}),
        } as APIGatewayEvent;
        const mockCollection = {} as Collection;
        const companiesService = new CompaniesService(
            mockEvent,
            mockCollection,
            user
        );
        try {
            companiesService.mapRequestToMethod();
        } catch (e) {
            const error = `We don't have a method to handle this request: /api/companies/59cd4bf28c25a93904f6d401/admin/59cd4bf28c25a93904f6d401/users`;
            expect(e.message).toMatch(error);
        }
    });

    test('Should make a company: ', async () => {
        const mockEvent = {
            path: '/api/companies',
            httpMethod: 'POST',
            body: JSON.stringify({
                _id: id,
                name: 'Test',
                phone: '4046700059',
                type: CompanyType.Carrier,
                address: {
                    line1: '',
                    line2: '',
                    line3: null,
                    city: 'my city',
                    state: 'jaha',
                    zip: '38477',
                },
                deleted: false,
                __v: 2,
            }),
        } as APIGatewayEvent;

        const companiesService = new CompaniesService(
            mockEvent,
            collection,
            user
        );
        const whichMethod = companiesService.mapRequestToMethod();
        const { result } = await companiesService[whichMethod]();
        expect(result.n).toBe(1);
        expect(result.ok).toBe(1);
    });

    test('Should update a company: ', async () => {
        const mockEvent = {
            path: `/api/companies/5fe939325f05741ddc814a95`,
            httpMethod: 'PUT',
            body: JSON.stringify({
                phone: '4046700060',
            }),
        } as APIGatewayEvent;

        const companiesService = new CompaniesService(
            mockEvent,
            collection,
            user
        );
        const whichMethod = companiesService.mapRequestToMethod();
        const company = await companiesService[whichMethod]();
        expect(company).toBeTruthy();
        // expect(result.ok).toBe(1);
    });

    test('Should delete a company: ', async () => {
        const mockEvent = {
            path: `/api/companies/${id}`,
            httpMethod: 'DELETE',
            body: JSON.stringify({
                phone: '4046700060',
            }),
        } as APIGatewayEvent;

        const companiesService = new CompaniesService(
            mockEvent,
            collection,
            user
        );
        const whichMethod = companiesService.mapRequestToMethod();
        const { result } = await companiesService[whichMethod]();
        expect(result.n).toBe(0);
        expect(result.ok).toBe(1);
    });
});

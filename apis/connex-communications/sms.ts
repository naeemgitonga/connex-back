import * as aws from 'aws-sdk';
import { CommunicationsResponse } from '../../declarations/communications';
const snsService = new aws.SNS({
    region: 'us-east-1',
});

export const sms = async (
    payload: {
        Message: string;
        PhoneNumber: string;
    },
    sns = snsService
): Promise<CommunicationsResponse> => {
    try {
        await sns.publish(payload).promise();
        return { status: 200, message: 'message being sent' };
    } catch (e) {
        return { status: 500, message: 'message not sent', error: e };
    }
};

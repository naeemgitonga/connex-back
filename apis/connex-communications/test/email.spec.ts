import { SES } from 'aws-sdk';
import email from '../email';
import { ObjectId } from 'mongodb';

describe('Email test suite', (): void => {
    test('Should send email message without error', async (): Promise<void> => {
        const payload = {
            email: 'I work',
            token: '40467000asdfasdfasdf59',
            id: new ObjectId(),
        };
        const ses = {
            sendEmail: _payload => ({
                promise: () => Promise.resolve({ MessageId: 'message1' }),
            }),
        };
        const res = await email(payload, (ses as unknown) as SES);
        expect(res.status).toBe(200);
        expect(res.message).toBe('Password reset email sent!');
    });

    test('Should not send sms message and err out', async (): Promise<void> => {
        console.log = jest.fn();
        const payload = {
            email: 'I work',
            token: '40467000asdfasdfasdf59',
            id: new ObjectId(),
        };
        const ses = {
            publish: _payload => Promise.resolve({ MessageId: 'message1' }),
        };
        const res = await email(payload, (ses as unknown) as SES);
        expect(res.status).toBe(500);
        expect(res.message).toBe('Password reset email could not be sent');
        expect('error' in res).toBeTruthy();
    });
});

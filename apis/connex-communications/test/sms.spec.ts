import { SNS } from 'aws-sdk';
import { sms } from '../sms';

describe('SMS test suite', (): void => {
    test('Should send sms message without error', async (): Promise<void> => {
        const payload = { Message: 'I work', PhoneNumber: '4046700059' };
        const sns = {
            publish: _payload => ({
                promise: () => Promise.resolve({ MessageId: 'message1' }),
            }),
        };
        const res = await sms(payload, (sns as unknown) as SNS);
        expect(res.status).toBe(200);
        expect(res.message).toBe('message being sent');
    });

    test('Should not send sms message and err out', async (): Promise<void> => {
        const payload = { Message: 'I work', PhoneNumber: '4046700059' };
        const sns = {
            publish: _payload => Promise.resolve({ MessageId: 'message1' }),
        };
        const res = await sms(payload, (sns as unknown) as SNS);
        expect(res.status).toBe(500);
        expect(res.message).toBe('message not sent');
        expect('error' in res).toBeTruthy();
    });
});

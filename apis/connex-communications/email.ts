import * as aws from 'aws-sdk';
import { ObjectId } from 'mongodb';

const sesService = new aws.SES({ apiVersion: '2010-12-01' });
import { CommunicationsResponse } from '../../declarations/communications';

export default async function email(
    payload: {
        email: string;
        token: string;
        id: ObjectId;
    },
    ses = sesService
): Promise<CommunicationsResponse> {
    const { email, token, id } = payload;
    const params = {
        Destination: {
            /* required */
            CcAddresses: ['support@connexapp.com'],
            ToAddresses: [
                email,
            ] /*for sandbox mode you must verify all emails */,
        },
        Message: {
            //* required */
            Body: {
                //* required */
                Html: {
                    Charset: 'UTF-8',
                    Data: template(token, id),
                },
            },
            Subject: {
                Charset: 'UTF-8',
                Data: 'Connex App Password Reset',
            },
        },
        Source: 'support@connexapp.com' /* required */,
        ReplyToAddresses: [
            /* more items */
        ],
    };
    try {
        const res = await ses.sendEmail(params).promise();
        return {
            status: 200,
            message: 'Password reset email sent!',
            MessageId: res.MessageId,
        };
    } catch (e) {
        console.log(e);
        return {
            status: 500,
            message: 'Password reset email could not be sent',
            error: e,
        };
    }
}

const template = (token: string, id: ObjectId): string => `
    <!doctype html>
    <html lang="en" id="html">
        <head>
            <style>
                a {
                    color: red !important;
                }
                p {
                    color:blue;
                    font-size: 30px;
                }
            </style>
        </head>
        <div>
            <h1>Password Reset</p>
            <h5>You have received this email from Connex App</h5>
            <p>This link is only valid for 15 minutes.</p>
            <p>Click <a href=${process.env.SENDTO}/reset-password?userId=${id}&resetToken=${token}>here</a> to reset password</p>
        </div>
    </html>
`;

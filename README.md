# Getting Started

## locally

To start this application locally

## What I learned

1. I had to set up a list of nearly 20 IP addresses so that my bitbucket pipelines
   would be able to interact with my Atlas MongoDB instance.

I also had to give my bitbucket user various IAM roles so that it would
be able to deploy my resources. So what I did there was create a group
called "Deployment". I attached the following policies:

AWSLambdaFullAccess
IAMFullAccess
AmazonAPIGatewayInvokeFullAccess
AmazonAPIGatewayAdministrator
AWSCloudFormationFullAccess

2. If you are repeatedly getting a `502` internal server error on your
   on your lambda instance and it is using MongoDB Atlas, you may want
   to check your useUnifiedTobpology setting and your ip whitelist.
   if you are using a sandbox the white list must allow connections from
   anywher `0.0.0.0/0.

## Common Errors

1. Missing handler:

    if you see this...

    ![handler error image](https://connex-imgs.s3.amazonaws.com/handler-error.png)

    be sure to add the handler to the [handler.ts](./handler.ts) file.

2. Renaming a resource:
   if you see this...

    ![yml error image](https://connex-imgs.s3.amazonaws.com/yml-error-1.png)

    At first I had this in the serverless.yml:

    ```
    - http
        path: company
        method: get
    -http
        path: companies/{proxy+}
        method: any
    ```

    Then I tried to change that to this:

    ```
    - http
        path: company/{proxy+}
        method: any
    -http
        path: companies/{proxy+}
        method: any
    ```

    That didn't work... so I removed the first one and just left
    the second after reading [this](https://github.com/serverless/serverless/issues/3785).

    Nothing worked. So I ended up tearing down everything
    since I haven't learned how to deploy and teardown
    one service at a time.

3. errors from serverless.yml
   if you see the following error:

    ![another serverless.yml error](https://connex-imgs.s3.amazonaws.com/yml-error-1.1.png)

    Make sure that you haven't jacked up your serverless.yml like this:

    ![file missing "heading"](https://connex-imgs.s3.amazonaws.com/yml-error-1.2.png)

    You see that this file is missing the `- http` header (i guess that's what you call it). When I added it back it worked again.

4. When you make a "greedy" path using `{proxy+}` always consider how
   you will name your routes. I encountered the infamous
   `Missing Auth Token` error because I was trying to use URL query
   strings with an "incomplete" greedy path. Here's an example:

    ```
    '/api/loads/{proxy+}'

    url with query string parameters:
    '/api/loads?type=abcd&create=true'
    ```

    The former will not work. It will cause that error previously
    described. The reason it will not work is because the query
    string parameter throws off the API Gateway.

    To resolve this simply fulfill the contract of the greedy path as follows and porblem solved:

    ```
    '/api/loads/create?type=abcd&create=true'
    ```

## SETUP

when setting up api gateway for the first time you will not be able to make successful request.
I have had to go in and enable cors for the `auth/login` endpoint and to set the header response to `200`. Also turn on logging in Resources >
Logs/Tracing.
Then redeploy the api to get it to work.

What I learned about API Gateway and headers. If you have a
"lambda proxy" integration, api gateway wil automatically pass
the entire request to the lambda. I was getting stuck because in
the cloud the request has different casing for the headers. I
was using `event.headers.cookie` which works locally but in the
cloud I was thinking that I wasn't getting my headers because
the proper casing was `event.headers.Cookie`. It led me down a path to discover ["integrtion passthrough behaviors"](https://docs.aws.amazon.com/apigateway/latest/developerguide/integration-passthrough-behaviors.html). I have been away from
lambdas doing other work for about 8 months and I had forgotten that.

So If you are looking to grab something from your event object in your lambda proxy
and it's coming back undefined, check the casing.

## Routing

I have created a routing mechanism/service to help with abstracting that code from the handler.

[BaseService](./apis/base-service.ts) is a generic class. It will be extended when you create a new
service and you will write your methods around the
basic methods you get from the base service.

One key method on the base service is `mapRequestToMethod`.
This method uses the route table that you create for your service, to properly route your request.

Here is an example route map found in the [CompaniesService](./apis/connex-companies/companies-service.ts)

```
 {
    GET: {
        getCompany: '/api/company',
        companiesUsersLoads: '/api/company/users/loads',
    },
    POST: {
        createCompany: '/api/company',
        companies: '/api/companies/:id'
    },
    PUT: {
        updateCompany: '/api/companies/:id'
    },
    DELETE: {
        deleteCompany: '/api/companies/:id'
    }
}
```

Ideally, each service will be a es6 `class`. It will, as mentioned previously,
will extend the [BaseService](./apis/base-service.ts). The new service should be designed
to accept the necessary data needed to process a request. It will take this data and
pass it along to the [BaseService](./apis/base-service.ts) so that the `mapRequestToMethod`
method can return you to you the method name that corresponds to the endpoing that
you are trying to consume.

For example, you want to do a `GET` request to get the company info that you are part of.
Well, in our handler we create a new instance of the service and pass it the event,
collection (that you want to interact with), and the user.

Once you do that you can call the `mapRequestToMethod` to determine which method you will
need. This method will return a string and from there you can call that method
on the instance of your service and await the response.

## 11-09-2020

Wrote an auth service to follow the convention of our classes and
base service inheritence. added new method to base service

Rewrote test cases. Got it all working.

Added user service and created test suite for user service.
updated login type def, merged two test suites, token
and authentication since their code now lives in connex-auth
renamed file login.spec.ts -> auth-service.spec.ts

alphabetized auth-service methods

## 11-10-2020

updated services to include new authentication methods from
AuthService, removed unncessary files for old auth methods,
all auth methods are now in auth-service.ts

added param handler to base service
updated test and aggpipeline for limits to begin pagination

## 11-11-2020

remaned pipeline file added address field to model for
company. Added loads handler logic and loads-service.ts
added loads to global enums collection enum added loads service
updated loads serverless.yml to include proxy+ in the route

## 11-12-2020

I started writing more test for response and mongo-connection.
I still need to write a couple of test for some methods on
my base service. however they are private methods. Jest is giving
me a not so great report as a result of that.

I broke out the inbound, outbound, and unsubmitted loads pipelines.
I had to update the handlers as a result of modifying the
mongo-connection code to test.

## 12-13-2020

updated agg pipeline for get all loads, it now will get
unsubmitted loads, created a function to create unsubmitted
loads. Added KMS encryption. This was added for env vars.
It works based on my IAM User. My IAM User has access keys and
is an administrator in AWS KMS. This setup isn't scaleable. What
if we had 30 engineers? The best way to do it is to have a Role
that has access to KMS and assign the role to the Users that need
it. Then you can give them Access Keys if they need them.

## 12-14-2020

updated aggregation pipeline to be multi-faceted. The facets allow us
to run multiple aggregations on the same input of data in a single
aggregation stage.

continued adding logic for pagination. made several new agg pipelines
and broke out any code that I could to make the pipeline stages
reusable.

counted the lines of code using `git ls-files | grep "\.ts$" | xargs wc -l`.

## 12-15-2020

added aggregation pipelines for pagination. deployed those changes.
wrote unit test

## 12-26-2020

fixed user options pipeline for user dropdown,
fixed some tests, fixed compare routes

## 12-26-2020

fixed user options pipeline for user dropdown,
fixed some tests, fixed compare routes

## 01-12-2021

Today, I learned that when doing an aggregation in mongodb
if you have bad data that could thow off your query and
cause a misleading error :![mongodb error screenshot](https://connex-imgs.s3.amazonaws.com/Screen+Shot+2021-01-12+at+7.31.33+AM.png)

The former error was thrown when running the following aggregation
pipeline on my bols collection:

```

db.getCollection('bols').aggregate([
    {
        $match: {
            carrierId: ObjectId('5fe9398b5f05741ddc814ab3'),
        },
    },
    {
        $lookup: {
            from: 'loads',
            as: 'load',
            let: { bolId: '$_id' },
            pipeline: [
                {
                    $match: {
                        $expr: {
                            $in: ['$$bolId', '$bols'],
                        },
                    },
                },
            ],
        },
    },
]);
```

Now, what's happening is that in my loads collection there are
some documents that do not have a bols property. When mongodb
encounters a document missing that property, it will
throw the former error. So the question is, how to run this query
on mongodb and not throw an error when `$bols` is `undefined`?

## 01-31-2021

today I continued with the back-end portion of the image
upload functionality. I had that set up in about 40 min or
less. I was able to use some code I found in a medium article
to bootstrap it.

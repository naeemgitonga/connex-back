module.exports = {
    bail: true,
    collectCoverage: true,
    coverageReporters: ['text-summary', 'lcov'],
    collectCoverageFrom: ['apis/*.ts', 'apis/**/*.ts', '!apis/**/handler.ts'],
    coveragePathIgnorePatterns: [
        '/node_modules/',
        'mocks',
        'declarations',
        'enums',
        'entities',
        'apis/kms-decryption.ts',
    ],
    verbose: true,
    roots: ['<rootDir>/apis'],
    transform: {
        '^.+\\.(t|j)sx?$': 'ts-jest',
    },
    reporters: ['default', ['jest-junit', { outputDirectory: 'test-results' }]],
    setupFiles: ['dotenv/config'],
};

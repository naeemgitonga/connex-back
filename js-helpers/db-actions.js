const { ObjectId } = require('mongodb');

function makeClerks() {
    var clerks = [];
    for (var i = 0; i < 25; i++) {
        clerks.push({
            username: 'clerk' + i + '@hotmail.com',
            password:
                '$2a$12$bkL1wrxMG9g/lyJX74pFbuX1.4.pogVfZ1ylTxL17MX/jLwgG2ZHC',
            firstName: 'Clerk' + i,
            img: '',
            lastName: 'User',
            phone: '404-670-0059',
            role: NumberInt(2),

            companyId: ObjectId('59cd4bf28c25a93904f6d401'),
            __v: NumberInt(0),
        });
    }
    db.getCollection('users').insertMany(clerks);
}

makeClerks();

function makeUnsubmittedLoads() {
    var loads = [];
    for (var i = 1; i < 25; i++) {
        loads.push({
            shipper: ObjectId('5fb203738634ff6fc02d9e02'),
            completed: false,
            driver: new ObjectId(),
            dropDate: new Date(),
            loadNum: 'aae3i23aeasf' + i,
            pUNum: '3492safodif' + i,
            deleted: false,
            pUDate: new Date(),
            dropDate: new Date(),
            ready: false,
            stops: NumberInt(0),
            createdBy: ObjectId('59cd4bf28c25a93904f6d401'),
            __v: NumberInt(2),
        });
    }
    db.getCollection('loads').insertMany(loads);
}

function makeOutboundLoads() {
    var loads = [];
    for (var i = 1; i < 25; i++) {
        loads.push({
            shipper: new ObjectId(),
            createdBy: ObjectId('59cd4bf28c25a93904f6d401'),
            completed: false,
            drivers: new ObjectId(),
            dropDate: new Date(),
            loadNum: 'aae3i23aeasf' + i,
            pUNum: '3492safodif' + i,
            deleted: false,
            pUDate: new Date(),
            dropDate: new Date(),
            ready: true,
            stops: NumberInt(0),
            destination: ObjectId('5fb203738634ff6fc02d9e02'),
            __v: NumberInt(2),
        });
    }
    db.getCollection('loads').insertMany(loads);
}

function makeShippers() {
    var companies = [];
    for (var i = 0; i < 25; i++) {
        companies.push({
            name: 'Test Company-' + i,
            phone: NumberInt(
                Math.floor(1000000000 + Math.random() * 9000000000)
            ),
            deleted: false,
            type: NumberInt(1),
            address: {
                line1: '123' + i + ' Fulton Industrial Blvd.',
                line2: '',
                line3: '',
                city: 'Atlanta',
                state: 'GA',
                zip: '30336',
            },
            __v: NumberInt(2),
        });
    }
    db.getCollection('companies').insertMany(companies);
}

makeShippers();

function makeAdmins() {
    var companies = db.getCollection('companies').find({}).toArray();
    var admins = [];
    for (var i = 0; i < companies.length; i++) {
        admins.push({
            username:
                'admin' +
                (i + 1) +
                companies[i].name.toLowerCase().replace(' ', '') +
                '@hotmail.com',
            password:
                '$2a$12$bkL1wrxMG9g/lyJX74pFbuX1.4.pogVfZ1ylTxL17MX/jLwgG2ZHC',
            firstName: 'Admin' + i,
            img: '',
            lastName: 'User',
            phone: '404-670-0059',
            role: NumberInt(1),

            companyId: companies[i]._id,
            __v: NumberInt(2),
        });
    }
    db.getCollection('users').insertMany(admins);
}

makeAdmins();

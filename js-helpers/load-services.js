var path = require('path');
const globby = require('globby');

/**
 * @param {Serverless} serverless
 */
exports.functions = async function (serverless) {
    const servicePath = serverless.config.servicePath;
    const paths = await globby([
        path.join(servicePath, './apis/*/serverless.yml'),
    ]);
    const files = paths.map(p =>
        serverless.yamlParser.parse(path.normalize(p))
    );
    const services = await Promise.all(files); //* serverless.yml will appear in an object
    return getFunctions(services);
};

/**
 * @param {ServerlessService[]} services
 */
async function getFunctions(services) {
    return services
        .filter(x => x && x.functions && typeof x.functions === 'object')
        .map(parseApp);
}

/**
 * @param {ServerlessService} appDefinition
 * @returns {FunctionMap}
 */
function parseApp(appDefinition) {
    const functionDefs = appDefinition.functions;
    if (Array.isArray(functionDefs)) {
        return Object.assign({}, ...functionDefs.map(x => fixFunctionMap(x)));
    }

    return fixFunctionMap(functionDefs);
}

/**
 * @param {FunctionMap} fns
 * @returns {FunctionMap}
 */
function fixFunctionMap(fns) {
    return Object.keys(fns).reduce((a, k) => {
        a[k] = fixFunctionDefinition(fns[k]);
        return a;
    }, {});
}

/**
 * @param {FunctionDefinition} lambdaDefinition
 */
function fixFunctionDefinition(lambdaDefinition) {
    const events = lambdaDefinition.events.map(x =>
        isHttpEvent(x) ? fixHttpEvent(x) : x
    );
    return { ...lambdaDefinition, events };
}

/**
 * @param {FunctionEvent} event
 * @returns {boolean}
 */
function isHttpEvent(event) {
    const keys = Object.keys(event);
    return keys.length === 1 && keys[0] === 'http';
}

/**
 * @param {FunctionHttpEvent} event
 * @returns {FunctionHttpEvent}
 */
function fixHttpEvent(event) {
    const body = event.http;
    const path = `/api/${body.path}`;
    return { http: { ...body, path } };
}

/**
 * @param {FunctionDefinition} lambdaDefinition
 */
function fixFunctionDefinition(lambdaDefinition) {
    const events = lambdaDefinition.events.map(x =>
        isHttpEvent(x) ? fixHttpEvent(x) : x
    );
    return { ...lambdaDefinition, events };
}

/**
 * @param {FunctionEvent} event
 * @returns {boolean}
 */
function isHttpEvent(event) {
    const keys = Object.keys(event);
    return keys.length === 1 && keys[0] === 'http';
}

/**
 * @param {FunctionHttpEvent} event
 * @returns {FunctionHttpEvent}
 */
function fixHttpEvent(event) {
    const body = event.http;
    const path = `/api/${body.path}`;
    return { http: { ...body, path } };
}

/**
 * @typedef {import('serverless')} Serverless
 */
/**
 * @typedef {Object} ServerlessService
 * @property {{stage: string}} provider
 * @property {FunctionMap|FunctionMap[]} functions
 */
/**
 * @typedef {{[key: string]: FunctionDefinition}} FunctionMap
 */
/**
 * @typedef {Object} FunctionDefinition
 * @property {string} handler
 * @property {FunctionEvent[]} events
 */
/**
 * @typedef {FunctionHttpEvent} FunctionEvent
 */
/**
 * @typedef {Object} FunctionHttpEvent
 * @property {FunctionHttpEventBody} http
 */
/**
 * @typedef {Object} FunctionHttpEventBody
 * @property {string} method
 * @property {string} path
 * @property {Object} request
 */

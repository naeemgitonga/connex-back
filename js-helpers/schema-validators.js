/**
 * * before applying these to the database
 * * you will likely need to run them through
 * * here: https://jsonformatter.curiousconcept.com/.
 * * that will format it for you. You can use compass
 * * or robo 3t (my personal fav). if using robo3t
 * * use this command:
 *      db.runCommand( {
            collMod: "recipes",
            validator: { $jsonSchema: {
                <<Validation Rules>>
                }
            }
        } )
 * * if you need a reference for your bsonTypes
 * * look here: https://docs.mongodb.com/manual/reference/operator/query/type/#document-type-available-types
 * * and you can find all of your types.
 * 
 * * here is a decent article about schema validation https://www.mongodb.com/blog/post/json-schema-validation--locking-down-your-model-the-smart-way
 * 
 */

const loads = {
    collMod: 'loads',
    validationLevel: 'moderate', //* if you don't use this (it will default to 'strict') or if you set it to strict your db will run validation on updates to existing documents and that will make it so that you have to send all required props with every update...too much
    validationAction: 'error',
    validator: {
        bsonType: 'object',
        required: ['ready', 'deleted', 'createdBy', 'pUDate', 'pUNum'],
        additionalProperties: true,
        properties: {
            _id: {},
            shipperId: {
                bsonType: 'objectId',
                description: 'id of the company shipping the load',
            },
            createdBy: {
                bsonType: 'objectId',
                description: 'id of person that created the load',
            },
            ready: {
                bsonType: 'bool',
                description: 'tells wether the load is ready to be shipped',
            },
            deleted: {
                bsonType: 'bool',
                description: 'used for a soft delete',
            },
            stops: {
                bsonType: 'int',
                description: 'number of stops on each load',
            },
            carrierId: {
                bsonType: 'objectId',
                description: 'id of company moving the load',
            },
            pUDate: {
                bsonType: 'date',
                description: 'date/time of pickup',
            },
            pUNum: {
                bsonType: 'string',
                description: 'number used by shipper, is of alphanumeric chars',
            },
            createdDate: {
                bsonType: 'date',
                description: 'date/time load was created',
            },
            bols: {
                bsonType: ['array'],
                items: {
                    bsonType: 'objectId',
                    additionalProperties: false,
                    description: 'id of bols associated with this load',
                },
            },
            checkin: {
                bsonType: ['null', 'date'],
                description: 'date/time of checkin for pickup',
            },
            checkout: {
                bsonType: ['null', 'date'],
                description: 'date/time for checkout of pickup',
            },
        },
    },
};

const bols = {
    collMod: 'bols',
    validationAction: 'error',
    validationLevel: 'moderate',
    validator: {
        $jsonSchema: {
            bsonType: 'object',
            required: ['file', 'checkin', 'checkout', 'dropDate'],
            additionalProperties: true,
            properties: {
                _id: {},
                file: {
                    bsonType: ['null', 'object'],
                    properties: {
                        lastModified: {
                            bsonType: ['int', 'double'],
                        },
                        lastModifiedDate: {
                            bsonType: ['string', 'date'],
                        },
                        name: {
                            bsonType: 'string',
                        },
                        size: {
                            bsonType: ['int', 'long'],
                        },
                        type: {
                            bsonType: 'string',
                        },
                        webkitRelativePath: {
                            bsonType: 'string',
                        },
                    },
                },
                shipperId: {
                    bsonType: 'objectId',
                },
                driverId: {
                    bsonType: 'objectId',
                },
                receiverId: {
                    bsonType: 'objectId',
                },
                checkin: {
                    bsonType: ['null', 'date'],
                },
                checkout: {
                    bsonType: ['null', 'date'],
                },
                dropDate: {
                    bsonType: 'date',
                },
                osd: {
                    bsonType: 'object',
                    properties: {
                        overages: {
                            bsonType: 'array',
                            minItems: 0,
                            items: {
                                bsonType: 'object',
                                properties: {
                                    sku: {
                                        bsonType: 'string',
                                    },
                                    qty: {
                                        bsonType: 'int',
                                    },
                                },
                            },
                        },
                        shortages: {
                            bsonType: 'array',
                            minItems: 0,
                            items: {
                                bsonType: 'object',
                                properties: {
                                    sku: {
                                        bsonType: 'string',
                                    },
                                    qty: {
                                        bsonType: 'int',
                                    },
                                },
                            },
                        },
                        damages: {
                            minItems: 0,
                            bsonType: 'array',
                            items: {
                                bsonType: 'object',
                                properties: {
                                    sku: {
                                        bsonType: 'string',
                                    },
                                    qty: {
                                        bsonType: 'int',
                                    },
                                },
                            },
                        },
                    },
                },
                imagePreviewUrl: {
                    bsonType: 'string',
                },
                sealIntact: {
                    bsonType: 'bool',
                },
            },
        },
    },
};

const companies = {
    collMod: 'companies',
    validationLevel: 'moderate',
    validationAction: 'error',
    validator: {
        $jsonSchema: {
            bsonType: 'object',
            required: ['name', 'phone', 'type', 'deleted', 'address'],
            additionalProperties: true,
            properties: {
                _id: {},
                address: {
                    bsonType: 'object',
                    properties: {
                        line1: {
                            bsonType: 'string',
                        },
                        line2: {
                            bsonType: ['null', 'string'],
                        },
                        line3: {
                            bsonType: ['null', 'string'],
                        },
                        city: {
                            bsonType: 'string',
                        },
                        state: {
                            bsonType: 'string',
                        },
                        zip: {
                            bsonType: 'string',
                        },
                    },
                },
                name: {
                    bsonType: 'string',
                },
                phone: {
                    bsonType: 'string',
                },
                type: {
                    bsonType: 'int',
                },
                deleted: {
                    bsonType: 'bool',
                },
            },
        },
    },
};

const users = {
    collMod: 'users',
    validationLevel: 'moderate',
    validationAction: 'error',
    validator: {
        $jsonSchema: {
            bsonType: 'object',
            required: [
                'username',
                'password',
                'firstName',
                'lastName',
                'phone',
                'img',
                'companyId',
                'role',
            ],
            additionalProperties: true,
            properties: {
                _id: {},
                username: {
                    bsonType: 'string',
                },
                password: {
                    bsonType: 'string',
                },
                firstName: {
                    bsonType: 'string',
                },
                lastName: {
                    bsonType: 'string',
                },
                phone: {
                    bsonType: 'string',
                },
                img: {
                    bsonType: ['null', 'string'],
                },
                companyId: {
                    bsonType: 'objectId',
                },
                role: {
                    bsonType: 'int',
                },
            },
        },
    },
};

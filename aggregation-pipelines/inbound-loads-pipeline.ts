import { ObjectId } from 'mongodb';

// * this is to be ran on the BOLs collection
export default function inboundLoadsPipeline(receiverId: ObjectId): Object[] {
    return [
        {
            $match: {
                $expr: {
                    $and: [
                        { receiver: receiverId }, // { $in: ['$receiver', [receiverId]] },
                        { $ne: ['$deleted', true] },
                    ],
                },
            },
        },
        { $skip: 0 }, //* Always apply 'skip' before 'limit'
        { $limit: 25 },
        {
            $lookup: {
                from: 'loads',
                as: 'load',
                localField: 'loadId',
                foreignField: '_id',
            },
        },
        { $unwind: '$load' },
        {
            $lookup: {
                from: 'companies',
                as: 'load.originCompany',
                localField: 'load.shipperId',
                foreignField: '_id',
            },
        },
        { $unwind: '$load.originCompany' },
    ];
}

import { ObjectId } from 'mongodb';

export function inboundFacet(getAll: boolean, page?: number): object {
    const $skip = getAll ? 0 : (page - 1) * 10;
    return {
        $facet: {
            count: [{ $count: 'count' }],
            docs: [
                { $skip }, //* Always apply 'skip' before 'limit'
                { $limit: 10 },
                {
                    $lookup: {
                        from: 'companies',
                        as: 'shipper',
                        localField: 'load.shipperId',
                        foreignField: '_id',
                    },
                },
                { $unwind: '$shipper' },
            ],
        },
    };
}

export function matchInbound(params: {
    id?: string;
    completed?: boolean;
}): object {
    const { id, completed } = params;
    let receiverId: string | ObjectId = '$$receiverId';
    if (id) {
        receiverId = new ObjectId(id);
    }
    return {
        $match: {
            $expr: {
                $and: [
                    completed
                        ? { $ne: ['$checkout', null] }
                        : { $eq: ['$checkout', null] },
                    { $eq: ['$receiverId', receiverId] },
                ],
            },
        },
    };
}

export function lookupInbound(): object {
    return {
        $lookup: {
            from: 'loads',
            as: 'load',
            localField: 'loadId',
            foreignField: '_id',
        },
    };
}

export function lookupCarrier(): object {
    return {
        $lookup: {
            from: 'companies',
            as: 'carrier',
            localField: 'load.carrierId',
            foreignField: '_id',
        },
    };
}

export function lookupDriver(): object {
    return {
        $lookup: {
            from: 'users',
            as: 'driver',
            localField: 'driverId',
            foreignField: '_id',
        },
    };
}

export function lookupLoad(): object {
    return {
        $lookup: {
            from: 'loads',
            as: 'load',
            localField: '_id',
            foreignField: 'bols',
        },
    };
}

import { unwind } from '../$unwind';
import { ObjectId } from 'mongodb';
import {
    inboundFacet,
    lookupCarrier,
    lookupDriver,
    lookupLoad,
    matchInbound,
} from './inbound-stages';

export default function getInbound(id: ObjectId, page = 1): object[] {
    return [
        { $match: { shipperId: new ObjectId(id) } },
        { $limit: 1 },
        {
            $lookup: {
                from: 'companies',
                as: 'company',
                localField: 'shipperId',
                foreignField: '_id',
            },
        },
        { $unwind: '$company' },
        { $project: { _id: '$company._id' } },
        {
            $lookup: {
                from: 'bols',
                as: 'inbound',
                let: { receiverId: '$_id' },
                pipeline: [
                    matchInbound({}),
                    lookupDriver(),
                    unwind('$driver'),
                    { $project: { 'driver.password': 0 } },
                    lookupLoad(),
                    unwind('$load'),
                    { $match: { 'load.ready': { $ne: false } } },
                    lookupCarrier(),
                    unwind('$carrier'),
                    inboundFacet(false, page),
                ],
            },
        },
        { $unwind: { path: '$inbound', preserveNullAndEmptyArrays: true } },
        {
            $unwind: {
                path: '$inbound.count',
                preserveNullAndEmptyArrays: true,
            },
        },
        {
            $project: {
                inboundTotal: '$inbound.count.count',
                inbound: '$inbound.docs',
            },
        },
    ];
}

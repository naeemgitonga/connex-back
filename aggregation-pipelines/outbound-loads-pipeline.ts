import { ObjectId } from 'mongodb';
import { outboundFacet } from './outbound-loads/outbound-stages';

//* this is to be ran on the loads collection
export default function outboundLoadsPipeline(shipperId: ObjectId): Object[] {
    return [
        {
            $match: {
                $expr: {
                    $and: [
                        { $ne: ['$ready', false] },
                        {
                            $eq: ['$shipper', shipperId],
                        },
                        { $ne: ['$deleted', true] },
                    ],
                },
            },
        },
        outboundFacet(true), //todo: make sure you want to hard code 'true' here
    ];
}

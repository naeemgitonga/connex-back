import { ObjectId } from 'mongodb';

export type QueryParams = {
    searchTerm: string;
    role: number;
    companyId: ObjectId;
};

// * this is only necessary to stop the ts(2322) error we would get when conditionally reassigning match
type Match = {
    $match: {
        companyId?: ObjectId;
        role: number;
        deleted?: object;
        $or: object[];
    };
};

export function usersOptionsPipeline(
    queryParams: QueryParams,
    searchAll: boolean
): object[] {
    const { companyId, role, searchTerm } = queryParams;
    let match: Match = {
        $match: {
            companyId,
            role,
            // deleted: { $ne: true },
            $or: [
                { firstName: { $regex: searchTerm, $options: 'i' } },
                { lastName: { $regex: searchTerm, $options: 'i' } },
                { username: { $regex: searchTerm, $options: 'i' } },
                { connexId: { $regex: searchTerm, $options: 'i' } },
            ],
        },
    };
    if (searchAll) {
        match = {
            $match: {
                role,
                deleted: { $ne: true },
                $or: [
                    { firstName: { $regex: searchTerm, $options: 'i' } },
                    { lastName: { $regex: searchTerm, $options: 'i' } },
                    { username: { $regex: searchTerm, $options: 'i' } },
                    { connexId: { $regex: searchTerm, $options: 'i' } },
                ],
            },
        };
    }

    return [
        {
            $project: {
                name: { $concat: ['$firstName', ' ', '$lastName'] },
                connexId: { $toString: '$_id' },
                companyId: 1,
                username: 1,
                phone: 1,
                role: 1,
                img: 1,
                firstName: 1,
                lastName: 1,
            },
        },
        match,
        {
            $project: {
                name: 1,
                connexId: { $substr: ['$connexId', 18, 24] },
                companyId: 1,
                username: 1,
                phone: 1,
                role: 1,
                img: 1,
            },
        },
    ];
}

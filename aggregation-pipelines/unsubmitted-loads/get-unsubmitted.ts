import { unwind } from '../$unwind';
import { ObjectId } from 'mongodb';
import { matchUnsubmitted, unsubmittedFacet } from './unsubmitted-stages';

export default function getUnsubmitted(id: ObjectId, page: number): object[] {
    return [
        matchUnsubmitted(id.toString()),
        unsubmittedFacet(false, page),
        unwind('$count'),
        {
            $project: {
                _id: 0,
                unsubmittedTotal: '$count.count',
                unsubmitted: '$docs',
            },
        },
    ];
}

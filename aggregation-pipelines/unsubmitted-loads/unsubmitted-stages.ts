import { createBolLookup } from '../lookups';
import { ObjectId } from 'mongodb';

export function unsubmittedFacet(getAll: boolean, page?: number): object {
    const $skip = getAll ? 0 : (page - 1) * 10;
    return {
        $facet: {
            count: [{ $count: 'count' }],
            docs: [
                { $skip }, //* Always apply 'skip' before 'limit'
                { $limit: 10 },
                createBolLookup(),
            ],
        },
    };
}

export function matchUnsubmitted(id?: string): object {
    let shipperId: string | ObjectId = '$$shipperId';
    if (id) {
        shipperId = new ObjectId(id);
    }
    return {
        $match: {
            $expr: {
                $and: [
                    { $ne: ['$ready', true] },
                    { $eq: ['$shipperId', shipperId] },
                    { $ne: ['$deleted', true] },
                ],
            },
        },
    };
}

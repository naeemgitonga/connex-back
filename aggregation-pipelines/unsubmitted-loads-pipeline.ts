import { ObjectId } from 'mongodb';

//* this is to be ran on the loads collection
export default function unsubmittedLoadsPipeline(
    shipperId: ObjectId
): Object[] {
    return [
        {
            $match: {
                $expr: {
                    $and: [
                        { $ne: ['$ready', true] },
                        { $eq: ['$shipper', shipperId] },
                        { $ne: ['$completed', true] },
                    ],
                },
            },
        },
        { $skip: 0 }, //* Always apply 'skip' before 'limit'
        { $limit: 25 },
        {
            $lookup: {
                from: 'companies',
                as: 'load.destination',
                localField: 'load.destination',
                foreignField: '_id',
            },
        },
        { $unwind: '$load.destination' },
    ];
}

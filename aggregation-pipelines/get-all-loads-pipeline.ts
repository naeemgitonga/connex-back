import { ObjectId } from 'mongodb';
import {
    inboundFacet,
    matchInbound,
    lookupCarrier,
    lookupDriver,
    lookupLoad,
} from './inbound-loads/inbound-stages';
import { outboundFacet } from './outbound-loads/outbound-stages';
import {
    unsubmittedFacet,
    matchUnsubmitted,
} from './unsubmitted-loads/unsubmitted-stages';
import { unwind } from './$unwind';
import { createLookup } from './lookups';

export function getAllLoadsPipeline(id: ObjectId): object[] {
    return [
        {
            $match: {
                _id: { $eq: new ObjectId(id) },
                deleted: { $ne: true },
            },
        },
        {
            $lookup: {
                from: 'bols',
                as: 'inbound',
                let: { receiverId: '$_id' },
                pipeline: [
                    matchInbound({}),
                    lookupDriver(),
                    unwind('$driver'),
                    { $project: { 'driver.password': 0 } },
                    lookupLoad(),
                    unwind('$load'),
                    { $match: { 'load.ready': { $ne: false } } },
                    lookupCarrier(),
                    unwind('$carrier'),
                    createLookup('companies', 'receiver', 'receiverId', '_id'),
                    unwind('$receiver'),
                    inboundFacet(true),
                ],
            },
        },
        unwind('$inbound'),
        unwind('$inbound.count'),
        {
            $lookup: {
                from: 'loads',
                as: 'outbound',
                let: { shipperId: '$_id' },
                pipeline: [
                    {
                        $match: {
                            $expr: {
                                $and: [
                                    { $ne: ['$ready', false] },
                                    { $eq: ['$shipperId', '$$shipperId'] },
                                    { $ne: ['$deleted', true] },
                                ],
                            },
                        },
                    },
                    outboundFacet(true),
                ],
            },
        },
        unwind('$outbound'),
        unwind('$outbound.count'),
        {
            $lookup: {
                from: 'loads',
                as: 'unsubmitted',
                let: { shipperId: '$_id' },
                pipeline: [matchUnsubmitted(), unsubmittedFacet(true)],
            },
        },
        unwind('$unsubmitted'),
        unwind('$unsubmitted.count'),
        {
            $lookup: {
                from: 'bols',
                as: 'completed',
                let: { companyId: '$_id' },
                pipeline: [
                    {
                        $match: {
                            $expr: {
                                $or: [
                                    { shipperId: '$$companyId' },
                                    { receiverId: '$$companyId' },
                                ],
                            },
                            driverId: { $exists: true },
                            checkout: { $ne: null },
                        },
                    },
                    createLookup('companies', 'receiver', 'receiverId', '_id'),
                    unwind('$receiver'),
                    createLookup('loads', 'load', '_id', 'bols'),
                    unwind('$load'),
                    createLookup('bols', 'load.bols', 'load.bols', '_id'),
                    {
                        $addFields: {
                            completed: {
                                $cond: {
                                    if: { $ne: ['$load.bols.checkout', null] },
                                    then: true,
                                    else: false,
                                },
                            },
                        },
                    },
                    createLookup('users', 'driver', 'driverId', '_id'),
                    unwind('$driver'),
                    { $project: { 'driver.password': 0 } },
                    createLookup(
                        'companies',
                        'carrier',
                        'load.carrierId',
                        '_id'
                    ),
                    unwind('$carrier'),
                    createLookup('companies', 'shipper', 'shipperId', '_id'),
                    unwind('$shipper'),
                ],
            },
        },
        {
            $project: {
                inboundTotal: '$inbound.count.count',
                inbound: '$inbound.docs',
                outboundTotal: '$outbound.count.count',
                outbound: '$outbound.docs',
                unsubmittedTotal: '$unsubmitted.count.count',
                unsubmitted: '$unsubmitted.docs',
                completed: {
                    $filter: {
                        input: '$completed',
                        as: 'bols',
                        cond: { $eq: ['$$bols.completed', true] },
                    },
                },
                completedTotal: { $size: '$completed' },
                _id: 0,
            },
        },
    ];
}

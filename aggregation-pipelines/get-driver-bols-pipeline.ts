import { ObjectId } from 'mongodb';
import { unwind } from './$unwind';
import { createLookup } from './lookups';

export default function getDriverBols(
    driverId: ObjectId,
    companyId: ObjectId
): object[] {
    return [
        {
            $match: {
                driverId,
                $or: [{ shipperId: companyId }, { receiverId: companyId }],
            },
        },
        createLookup('loads', 'load', '_id', 'bols'),
        unwind('$load'),
    ];
}

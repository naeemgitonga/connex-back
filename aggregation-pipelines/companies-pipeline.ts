import { ObjectId } from 'mongodb';

export function makeCompanyPipeline(
    id: ObjectId,
    getLoads?: boolean
): object[] {
    if (getLoads) {
        return [
            {
                $match: {
                    _id: { $eq: new ObjectId(id) },
                    deleted: { $ne: true },
                },
            },
            {
                $lookup: {
                    from: 'users',
                    as: 'admin',
                    let: { id: '$_id' },
                    pipeline: [
                        {
                            $project: {
                                name: {
                                    $concat: ['$firstName', ' ', '$lastName'],
                                },
                                connexId: { $toString: '$_id' },
                                companyId: 1,
                                username: 1,
                                phone: 1,
                                role: 1,
                                img: 1,
                                firstName: 1,
                                lastName: 1,
                            },
                        },
                        {
                            $match: {
                                $expr: {
                                    $and: [
                                        { $eq: ['$role', 1] },
                                        { $eq: ['$companyId', '$$id'] },
                                    ],
                                },
                            },
                        },
                        {
                            $project: {
                                name: 1,
                                connexId: { $substr: ['$connexId', 18, 24] },
                                companyId: 1,
                                username: 1,
                                phone: 1,
                                role: 1,
                                img: 1,
                            },
                        },
                    ],
                },
            },
            {
                $lookup: {
                    from: 'users',
                    as: 'clerks',
                    let: { id: '$_id' },
                    pipeline: [
                        {
                            $project: {
                                name: {
                                    $concat: ['$firstName', ' ', '$lastName'],
                                },
                                connexId: { $toString: '$_id' },
                                companyId: 1,
                                username: 1,
                                phone: 1,
                                role: 1,
                                img: 1,
                                firstName: 1,
                                lastName: 1,
                            },
                        },
                        {
                            $match: {
                                $expr: {
                                    $and: [
                                        { $eq: ['$role', 2] },
                                        { $eq: ['$companyId', '$$id'] },
                                    ],
                                },
                            },
                        },
                        {
                            $project: {
                                name: 1,
                                connexId: { $substr: ['$connexId', 18, 24] },
                                companyId: 1,
                                username: 1,
                                phone: 1,
                                role: 1,
                                img: 1,
                            },
                        },
                        { $skip: 0 }, //* Always apply 'skip' before 'limit'
                        { $limit: 25 },
                        {
                            $project: {
                                name: 1,
                                connexId: { $substr: ['$connexId', 18, 24] },
                                companyId: 1,
                                username: 1,
                                phone: 1,
                                role: 1,
                                img: 1,
                            },
                        },
                    ],
                },
            },
            {
                $lookup: {
                    from: 'users',
                    as: 'drivers',
                    let: { id: '$_id' },
                    pipeline: [
                        {
                            $project: {
                                name: {
                                    $concat: ['$firstName', ' ', '$lastName'],
                                },
                                connexId: { $toString: '$_id' },
                                companyId: 1,
                                username: 1,
                                phone: 1,
                                role: 1,
                                img: 1,
                                firstName: 1,
                                lastName: 1,
                            },
                        },
                        {
                            $match: {
                                $expr: {
                                    $and: [
                                        { $eq: ['$role', 3] },
                                        { $eq: ['$companyId', '$$id'] },
                                    ],
                                },
                            },
                        },
                        { $skip: 0 }, //* Always apply 'skip' before 'limit'
                        { $limit: 25 },
                        {
                            $project: {
                                name: 1,
                                connexId: { $substr: ['$connexId', 18, 24] },
                                companyId: 1,
                                username: 1,
                                phone: 1,
                                role: 1,
                                img: 1,
                            },
                        },
                    ],
                },
            },
            ...getAllLoads(),
            {
                $project: {
                    'admin.password': 0,
                    'clerks.password': 0,
                    'drivers.password': 0,
                },
            },
        ];
    }

    return [
        {
            $match: {
                _id: { $eq: new ObjectId(id) },
                deleted: { $ne: true },
            },
        },
        {
            $lookup: {
                from: 'users',
                as: 'admin',
                let: { id: '$_id' },
                pipeline: [
                    {
                        $project: {
                            name: {
                                $concat: ['$firstName', ' ', '$lastName'],
                            },
                            connexId: { $toString: '$_id' },
                            companyId: 1,
                            username: 1,
                            phone: 1,
                            role: 1,
                            img: 1,
                            firstName: 1,
                            lastName: 1,
                        },
                    },
                    {
                        $match: {
                            $expr: {
                                $and: [
                                    { $eq: ['$role', 1] },
                                    { $eq: ['$companyId', '$$id'] },
                                ],
                            },
                        },
                    },
                    {
                        $project: {
                            name: 1,
                            connexId: { $substr: ['$connexId', 18, 24] },
                            companyId: 1,
                            username: 1,
                            phone: 1,
                            role: 1,
                            img: 1,
                        },
                    },
                ],
            },
        },
        {
            $lookup: {
                from: 'users',
                as: 'clerks',
                let: { id: '$_id' },
                pipeline: [
                    {
                        $project: {
                            name: {
                                $concat: ['$firstName', ' ', '$lastName'],
                            },
                            connexId: { $toString: '$_id' },
                            companyId: 1,
                            username: 1,
                            phone: 1,
                            role: 1,
                            img: 1,
                            firstName: 1,
                            lastName: 1,
                        },
                    },
                    {
                        $match: {
                            $expr: {
                                $and: [
                                    { $eq: ['$role', 2] },
                                    { $eq: ['$companyId', '$$id'] },
                                ],
                            },
                        },
                    },
                    { $skip: 0 }, //* Always apply 'skip' before 'limit'
                    { $limit: 25 },
                    {
                        $project: {
                            name: 1,
                            connexId: { $substr: ['$connexId', 18, 24] },
                            companyId: 1,
                            username: 1,
                            phone: 1,
                            role: 1,
                            img: 1,
                        },
                    },
                ],
            },
        },
        {
            $lookup: {
                from: 'users',
                as: 'drivers',
                let: { id: '$_id' },
                pipeline: [
                    {
                        $project: {
                            name: {
                                $concat: ['$firstName', ' ', '$lastName'],
                            },
                            connexId: { $toString: '$_id' },
                            companyId: 1,
                            username: 1,
                            phone: 1,
                            role: 1,
                            img: 1,
                            firstName: 1,
                            lastName: 1,
                        },
                    },
                    {
                        $match: {
                            $expr: {
                                $and: [
                                    { $eq: ['$role', 3] },
                                    { $eq: ['$companyId', '$$id'] },
                                ],
                            },
                        },
                    },
                    { $skip: 0 }, //* Always apply 'skip' before 'limit'
                    { $limit: 25 },
                    {
                        $project: {
                            name: 1,
                            connexId: { $substr: ['$connexId', 18, 24] },
                            companyId: 1,
                            username: 1,
                            phone: 1,
                            role: 1,
                            img: 1,
                        },
                    },
                ],
            },
        },
        {
            $project: {
                'admin.password': 0,
                'clerks.password': 0,
                'drivers.password': 0,
            },
        },
    ];
}

function getAllLoads(): object[] {
    return [
        {
            $lookup: {
                from: 'bols',
                as: 'inbound',
                let: { receiverId: '$_id' },
                pipeline: [
                    {
                        $match: {
                            $expr: {
                                $and: [
                                    { receiver: '$$receiverId' },
                                    { $ne: ['$deleted', true] },
                                ],
                            },
                        },
                    },
                    { $skip: 0 }, //* Always apply 'skip' before 'limit'
                    { $limit: 25 },
                    {
                        $lookup: {
                            from: 'loads',
                            as: 'load',
                            localField: 'loadId',
                            foreignField: '_id',
                        },
                    },
                    { $unwind: '$load' },
                    {
                        $lookup: {
                            from: 'companies',
                            as: 'load.originCompany',
                            localField: 'load.shipperId',
                            foreignField: '_id',
                        },
                    },
                    { $unwind: '$load.originCompany' },
                ],
            },
        },
        {
            $lookup: {
                from: 'loads',
                as: 'outbound',
                let: { shipperId: '$_id' },
                pipeline: [
                    {
                        $match: {
                            $expr: {
                                $and: [
                                    { $ne: ['$ready', false] },
                                    { $eq: ['$shipper', '$$shipperId'] },
                                    { $ne: ['$deleted', true] },
                                ],
                            },
                        },
                    },
                    { $skip: 0 }, //* Always apply 'skip' before 'limit'
                    { $limit: 25 },
                    {
                        $lookup: {
                            from: 'companies',
                            as: 'final',
                            localField: 'destination',
                            foreignField: '_id',
                        },
                    },
                    { $unwind: '$final' },
                ],
            },
        },
        {
            $lookup: {
                from: 'loads',
                as: 'unsubmitted',
                let: { createdById: '$_id' },
                pipeline: [
                    {
                        $match: {
                            $expr: {
                                $and: [
                                    { $ne: ['$ready', true] },
                                    { createdBy: '$$createdById' },
                                    { $ne: ['$completed', true] },
                                ],
                            },
                        },
                    },
                    { $skip: 0 }, //* Always apply 'skip' before 'limit'
                    { $limit: 25 },
                    {
                        $lookup: {
                            from: 'companies',
                            as: 'load.destination',
                            localField: 'load.destination',
                            foreignField: '_id',
                        },
                    },
                    { $unwind: '$load.destination' },
                ],
            },
        },
    ];
}
// "drivers" : [
//     ObjectId("5a864fa377de55437009ab25")
// ],

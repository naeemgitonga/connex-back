import { unwind } from './$unwind';

export function createLookup(
    from: string,
    as: string,
    localField: string,
    foreignField: string
): object {
    return {
        $lookup: {
            from,
            as,
            localField,
            foreignField,
        },
    };
}

/**
 * @description used for looking up bols in outbound, unsubmitted, and carrier loads
 * these loads are actually the load in contrast to inbound and some completed loads
 */
export function createBolLookup(preserveDriverId = false): object {
    return {
        $lookup: {
            from: 'bols',
            as: 'bols',
            let: { bolIds: '$bols' },
            pipeline: [
                {
                    $match: {
                        $expr: {
                            $in: ['$_id', '$$bolIds'],
                        },
                    },
                },
                createLookup('companies', 'receiver', 'receiverId', '_id'),
                unwind('$receiver'),
                createLookup('users', 'driver', 'driverId', '_id'),
                unwind('$driver'),
                {
                    $project: {
                        receiverId: 1,
                        shipperId: 1,
                        checkin: 1,
                        driverId: 1,
                        checkout: 1,
                        dropDate: 1,
                        driver: {
                            name: {
                                $concat: [
                                    '$driver.firstName',
                                    ' ',
                                    '$driver.lastName',
                                ],
                            },
                            img: '$driver.img',
                            phone: '$driver.phone',
                            email: '$driver.username',
                            role: '$driver.role',
                            _id: { $toString: '$driver._id' }, //todo:  fix connexId for driver
                        },
                        file: 1,
                        imagePreviewUrl: 1,
                        osd: 1,
                        receiver: {
                            address: 1,
                            name: 1,
                            phone: 1,
                            type: 1,
                        },
                    },
                },
                {
                    $project: {
                        receiverId: 1, // * without this editing a load will give the bol a new receiverId
                        shipperId: 1, // * ditto
                        driverId: {
                            $cond: {
                                if: { $eq: [preserveDriverId, false] },
                                then: '$$REMOVE',
                                else: '$driverId',
                            },
                        },
                        checkin: 1,
                        checkout: 1,
                        dropDate: 1,
                        driver: {
                            name: '$driver.name',
                            img: '$driver.img',
                            phone: '$driver.phone',
                            email: '$driver.username',
                            role: '$driver.role',
                            connexId: {
                                $substr: ['$driver._id', 18, 24],
                            },
                            _id: '$driver._id',
                        },
                        file: 1,
                        imagePreviewUrl: 1,
                        osd: 1,
                        receiver: {
                            address: 1,
                            name: 1,
                            phone: 1,
                            type: 1,
                        },
                    },
                },
            ],
        },
    };
}

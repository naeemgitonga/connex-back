import { ObjectId } from 'mongodb';
import { unwind } from './$unwind';
import { createBolLookup, createLookup } from './lookups';

export default function driverLoads(driverId: ObjectId): object[] {
    return [
        {
            $match: {
                deleted: { $ne: true },
                ready: { $eq: true },
            },
        },
        createBolLookup(true),
        {
            $match: {
                'bols.driverId': driverId,
            },
        },
        createLookup('companies', 'shipper', 'shipperId', '_id'),
        createLookup('users', 'creator', 'createdBy', '_id'),
        unwind('$creator'),
        unwind('$shipper'),
        {
            $addFields: {
                completed: { $allElementsTrue: ['$bols.checkout'] },
            },
        },
        {
            $project: {
                stops: 1,
                completed: 1,
                shipper: {
                    address: '$shipper.address',
                    name: '$shipper.name',
                    phone: '$shipper.phone',
                    type: '$shipper.type',
                },
                ready: 1,
                pUNum: 1,
                pUDate: 1,
                creator: {
                    name: {
                        $concat: [
                            '$creator.firstName',
                            ' ',
                            '$creator.lastName',
                        ],
                    },
                    img: '$creator.img',
                    phone: '$creator.phone',
                    email: '$creator.username',
                    role: '$creator.role',
                },
                bols: 1,
                checkin: 1,
                checkout: 1,
                _id: 1,
            },
        },
        {
            $facet: {
                completedDocs: [
                    { $skip: 0 }, //* Always apply 'skip' before 'limit'
                    { $limit: 10 },
                    {
                        $match: {
                            completed: true,
                        },
                    },
                ],
                shippingDocs: [
                    { $skip: 0 }, //* Always apply 'skip' before 'limit'
                    { $limit: 10 },
                    {
                        $match: {
                            completed: false,
                        },
                    },
                ],
            },
        },
        {
            $project: {
                completed: '$completedDocs',
                completedTotal: { $size: '$completedDocs' },
                shipping: '$shippingDocs',
                shippingTotal: { $size: '$shippingDocs' },
            },
        },
    ];
}

export function unwind(path: string): object {
    return {
        $unwind: {
            path: path,
            preserveNullAndEmptyArrays: true,
        }
    }
}

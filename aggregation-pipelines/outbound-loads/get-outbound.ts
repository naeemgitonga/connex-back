import { ObjectId } from 'mongodb';
import { matchOutbound, outboundFacet } from './outbound-stages';

export default function getOutbound(id: ObjectId, page: number): object[] {
    return [
        matchOutbound(id.toString()),
        outboundFacet(false, page),
        { $unwind: { path: '$count', preserveNullAndEmptyArrays: true } },
        { $project: { outboundTotal: '$count.count', outbound: '$docs' } },
    ];
}

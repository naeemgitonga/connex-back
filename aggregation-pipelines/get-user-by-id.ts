import { ObjectId } from 'mongodb';

export default function getUserByIdPipeline(_id: ObjectId): object[] {
    return [
        { $match: { _id } },
        {
            $project: {
                _id: 1,
                firstName: 1,
                lastName: 1,
                name: { $concat: ['$firstName', ' ', '$lastName'] },
                connexId: { $toString: '$_id' },
                username: 1,
                phone: 1,
                role: 1,
                img: 1,
                email: '$username',
                companyId: 1,
            },
        },
        {
            $project: {
                _id: 1,
                firstName: 1,
                lastName: 1,
                name: 1,
                connexId: { $substr: ['$connexId', 18, 24] },
                username: 1,
                phone: 1,
                role: 1,
                img: 1,
                email: '$username',
                companyId: 1,
            },
        },
    ];
}

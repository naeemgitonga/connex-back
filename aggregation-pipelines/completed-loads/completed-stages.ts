import { ObjectId } from 'mongodb';

export function matchCompleted(params: { id?: string }): object {
    const { id } = params;
    let receiverId: string | ObjectId = '$$receiverId';
    if (id) {
        receiverId = new ObjectId(id);
    }
    return {
        $match: {
            $expr: {
                $and: [
                    { $ne: ['$checkout', null] },
                    {
                        $or: [
                            { $eq: ['$receiverId', receiverId] },
                            { $eq: ['$shipperId', receiverId] },
                        ],
                    },
                ],
            },
        },
    };
}

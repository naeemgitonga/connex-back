import { unwind } from '../$unwind';
import { createLookup } from '../lookups';
import { ObjectId } from 'mongodb';

/**
 * @description can be used to get completed loads and completed paginated loads
 */
export default function getCompleted(
    id: ObjectId,
    page: number,
    getAll = false
): object[] {
    const $skip = getAll ? 0 : (page - 1) * 10;
    return [
        {
            $match: {
                $expr: {
                    $or: [
                        { shipperId: new ObjectId(id.toString()) },
                        { receiverId: new ObjectId(id.toString()) },
                    ],
                },
                driverId: { $exists: true },
                checkout: { $ne: null },
            },
        },
        createLookup('companies', 'receiver', 'receiverId', '_id'),
        unwind('$receiver'),
        createLookup('loads', 'load', '_id', 'bols'),
        unwind('$load'),
        createLookup('bols', 'load.bols', 'load.bols', '_id'),
        {
            $addFields: {
                completed: {
                    $allElementsTrue: ['$load.bols.checkout'],
                },
            },
        },
        createLookup('users', 'driver', 'driverId', '_id'),
        unwind('$driver'),
        { $project: { 'driver.password': 0 } },
        createLookup('companies', 'carrier', 'load.carrierId', '_id'),
        unwind('$carrier'),
        createLookup('companies', 'shipper', 'shipperId', '_id'),
        unwind('$shipper'),
        {
            $facet: {
                completedTotal: [{ $count: 'count' }],
                completed: [
                    { $skip }, //* Always apply 'skip' before 'limit'
                    { $limit: 10 },
                ],
            },
        },
        unwind('$completedTotal'),
        {
            $project: {
                completedTotal: '$completedTotal.count',
                completed: 1,
            },
        },
    ];
}
